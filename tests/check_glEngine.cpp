#include <cassert>
#include <iostream>

#include <boost/make_shared.hpp>

#include "glEngine.h"
#include <glm/ext.hpp>

using namespace std;
using namespace posture;

vector<glm::vec4> generateNoise(const float xMax, const float yMax, const float zMax, const size_t size)
{
    // Initialize some data
    vector<glm::vec4> noise;
    noise.reserve(size);
    for (size_t i = 0; i < size; ++i)
    {
        int plane = rand() % 3 + 1;
        float sign = round(((float)rand() / RAND_MAX)) * 2.0f - 1.0f;
        float x,y,z;

        switch (plane)
        {
            case 1:
            {
                x = ((float)rand() / RAND_MAX)*0.01+(xMax/2.0f) * sign;
                y = (float)rand() / RAND_MAX*yMax-(yMax/2.0f);
                z = (float)rand() / RAND_MAX*zMax-(zMax/2.0f);
            }
            break;
            case 2:
            {
                x = (float)rand() / RAND_MAX*xMax-(xMax/2.0f);
                y = ((float)rand() / RAND_MAX)*0.01+(yMax/2.0f) * sign;
                z = (float)rand() / RAND_MAX*zMax-(zMax/2.0f);
            }
            break;
            case 3:
            {
                x = (float)rand() / RAND_MAX*xMax-(xMax/2.0f);
                y = (float)rand() / RAND_MAX*yMax-(yMax/2.0f);
                z = ((float)rand() / RAND_MAX)*0.01+(zMax/2.0f) * sign;
            }
            break;
        }

        glm::vec4 point(x, y, z, 1.0f);
        noise.push_back(point);
    }

    return noise;
}

vector<glm::vec4> generateTriangle()
{
    vector<glm::vec4> vertices;
    vertices.push_back(glm::vec4(-0.5f, -0.5f, 0.0f, 1.0f));
    vertices.push_back(glm::vec4(0.5f, -0.5f, 0.0f, 1.0f));
    vertices.push_back(glm::vec4(0.0f, 0.5f, 0.0f, 1.0f));

    return vertices;
}

vector<glm::vec4> generateSquare()
{
    float depth = 0.0f;
    vector<glm::vec4> vertices;
    vertices.push_back(glm::vec4(-0.5f, -0.5f, depth, 1.0f));
    vertices.push_back(glm::vec4(0.5f, -0.5f, depth, 1.0f));
    vertices.push_back(glm::vec4(-0.5f, 0.5f, depth, 1.0f));

    vertices.push_back(glm::vec4(0.5f, -0.5f, depth, 1.0f));
    vertices.push_back(glm::vec4(0.5f, 0.5f, depth, 1.0f));
    vertices.push_back(glm::vec4(-0.5f, 0.5f, depth, 1.0f));

    return vertices;
}

/*************/
int main(int argc, char** argv)
{
    auto glEngine = std::make_shared<GLEngine>();

    auto shader = make_shared<Shader>(glEngine);
    auto pathTemplate = string("shaders/render");
    shader->setSourceFromFile(pathTemplate + ".vs", Shader::vertex);
    shader->setSourceFromFile(pathTemplate + ".fs", Shader::fragment);
    shader->compileProgram();

    glEngine->setShader(shader);
    glEngine->setBufferNumber(1);

    std::vector<glm::vec4> noise = generateNoise(1.0f, 1.0f, 1.0f, 1000);
    //vector<glm::vec4> square = generateSquare();
    //vector<glm::vec4> triangle = generateTriangle();
    GLsizei elemSize = sizeof(glm::vec4)/sizeof(float);
    GLsizei bufferSize = noise.size()*sizeof(glm::vec4);
    //std::clog << "size: " << bufferSize << " (" << square.size() << " * " << elemSize << ")" << std::endl;
    glEngine->setBuffer(noise.data(), bufferSize, sizeof(float), 0, elemSize, GL_FLOAT);

    for (auto i = 0; i < 100; ++i)
        glEngine->run();

    return 0;
}
