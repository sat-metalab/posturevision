#include <cassert>
#include <chrono>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <thread>

#include <boost/make_shared.hpp>

#include "zcamera.h"
#include "display.h"

using namespace std;

/*************/
int main(int argc, char** argv)
{
    auto cloud = boost::make_shared<pcl::PointCloud<pcl::PointXYZRGBNormal>>();
    auto cloudSerialised = vector<char>();

    auto mesh = boost::make_shared<pcl::PolygonMesh>();
    auto meshSerialized = vector<unsigned char>();

    {
        auto disp = std::unique_ptr<posture::Display> (new posture::Display ("cloud"));
        posture::ZCamera::getNoise (1, 1, 1, 512, cloud);
        disp->setInputCloud (cloud);
        this_thread::sleep_for(chrono::seconds(2));
    }

    {
        auto disp = std::unique_ptr<posture::Display> (new posture::Display ("serialized cloud"));
        posture::ZCamera::getNoise (1, 1, 1, 512, cloudSerialised);
        disp->setInputCloud (cloudSerialised, false);
        this_thread::sleep_for(chrono::seconds(2));
    }

    {
        auto disp = std::unique_ptr<posture::Display> (new posture::Display ("mesh"));
        posture::ZCamera::getRandomMesh (1, 1, 1, 12, mesh);
        disp->setPolygonMesh (mesh);
        this_thread::sleep_for(chrono::seconds(2));
    }

    {
        auto disp = std::unique_ptr<posture::Display> (new posture::Display ("mesh"));
        posture::ZCamera::getRandomMesh (1, 1, 1, 12, meshSerialized);
        disp->setPolygonMesh (meshSerialized);
        this_thread::sleep_for(chrono::seconds(2));
    }

    return 0;
}
