#include <cassert>
#include <chrono>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <thread>

#include <boost/make_shared.hpp>

#include "solidifyGPU.h"
#include "zcamera.h"

using namespace std;

/*************/
int main(int argc, char** argv)
{
    auto zcamera = posture::ZCamera();
    auto mesh_creator = posture::SolidifyGPU();

    std::vector<unsigned char> depth;
    std::vector<unsigned char> mesh;
    unsigned int width, height;
    zcamera.getDepthImage(depth, width, height);

    mesh_creator.setDepthMapNbr(1);
    mesh_creator.setGridSize(1);
    mesh_creator.setGridResolution(16);
    mesh_creator.setInputDepthMap(0, depth, width, height);
    mesh_creator.getMesh(mesh);
}
