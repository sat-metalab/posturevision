#include <cassert>
#include <chrono>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <thread>

#include <boost/make_shared.hpp>

#include "calibrationreader.h"
#include "zcamera.h"
#include "meshmerger.h"
#include "solidify.h"

using namespace std;

/*************/
int main(int argc, char** argv)
{
    auto mesh1 = boost::make_shared<pcl::PolygonMesh>();
    auto mesh2 = boost::make_shared<pcl::PolygonMesh>();
    auto mergedMesh = boost::make_shared<pcl::PolygonMesh>();

    posture::ZCamera::getRandomMesh(1, 1, 1, 10, mesh1);
    posture::ZCamera::getRandomMesh(1, 1, 1, 10, mesh2);

    auto calibrationReader = posture::CalibrationReader("default.kvc");
    auto merger = posture::MeshMerger(2);
    merger.setCalibration(calibrationReader.getCalibrationParams());

    merger.start();
    merger.setInputMesh(0, mesh1);
    merger.setInputMesh(1, mesh2);
    merger.getMesh(mergedMesh);
    merger.stop();

    assert(mergedMesh->polygons.size() > 0);
    assert(mergedMesh->polygons.size() == mesh1->polygons.size() + mesh2->polygons.size());

    // serialised test

    auto meshSerialized1 = vector<unsigned char>();
    auto meshSerialized2 = vector<unsigned char>();
    auto mergedMeshSerialized = vector<unsigned char>();

    posture::ZCamera::getRandomMesh(1, 1, 1, 10, meshSerialized1);
    posture::ZCamera::getRandomMesh(1, 1, 1, 10, meshSerialized2);

    merger.start();
    merger.setInputMesh(0, meshSerialized1);
    merger.setInputMesh(1, meshSerialized2);
    merger.getMesh(mergedMeshSerialized);
    merger.stop();

    assert(mergedMeshSerialized.size() > 0);
    assert(mergedMeshSerialized.size() == meshSerialized1.size() + meshSerialized2.size() - 8);

    return 0;
}
