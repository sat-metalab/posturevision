/*
 * Copyright (C) 2014 Emmanuel Durand
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @detect-impl.h
 * The DetectImpl class
 */

#ifndef __DETECT_IMPL_H__
#define __DETECT_IMPL_H__

#include <string>
#include <memory>
#include <mutex>
#include <utility>
#include <vector>

#include <pcl/filters/extract_indices.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/surface/convex_hull.h>
#include <pcl/surface/concave_hull.h>

#include "config.h"
#include "constants.h"
#include "pointcloudserializer.h"
#include "meshserializer.h"
#include "shapes.h"

namespace posture
{
    /*************/
    //! DetectImpl class
    class DetectImpl
    {
        public:
            /**
             * \brief Constructor
             */
            DetectImpl();
    
            /**
             * \brief Destructor
             */
            ~DetectImpl();

            /**
             * \brief Do the detection process
             * \return True if something was detected
             */
            bool detect();

            /**
             * \brief Get the input cloud, with the detected shapes colored
             * \param cloud Pointer on the structure where to write the output point cloud
             * \return Timestamp for the grab (in ms, from epoch)
             */
            unsigned long long getProcessedCloud(std::vector<char>& cloud);
            unsigned long long getProcessedCloud(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud);

            /**
             * \brief Get a convex hull poly for the detected cloud specified by the index
             * \param index index of the cloud
             * \param mesh Pointer on the structure where to write the output mesh
             * \return Timestamp for the grab (in ms, from epoch)
             */
            unsigned long long getConvexHull (std::vector<unsigned char> mesh, unsigned int index);
            unsigned long long getConvexHull (pcl::PolygonMesh::Ptr mesh, unsigned int index);

            /**
             * \brief Get a vector containing the detected shapes
             * \return the shape vector
             */
            std::vector<Shape> getDetectedShapes();
    
            /**
             * \brief Set one of the input clouds
             * \param cloud Point cloud
             * \param compressed true if the cloud is compressed
             */
            void setInputCloud(const std::vector<char>& cloud, const bool compressed);
            void setInputCloud(const pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr& cloud);
    
        private:
            std::mutex _mutex;
            PointCloudSerializer<pcl::PointXYZRGBNormal> _serializer;
            MeshSerializer _meshSerializer;

            pcl::SACSegmentationFromNormals<pcl::PointXYZRGBNormal, pcl::PointXYZRGBNormal> _sacSegmentation;
            pcl::ExtractIndices<pcl::PointXYZRGBNormal> _extract;
            pcl::ConcaveHull<pcl::PointXYZRGBNormal> _hull;

            unsigned long long _timestamp;
            pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr _inputCloud;

            typedef std::pair<pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr, pcl::ModelCoefficients::Ptr> DetectedCloud;
            std::vector<DetectedCloud> _detectedCloud;

            unsigned int _maxObjects {5};
            unsigned int _minPoints {500};
    };

} // end of namespace

#endif // DETECT_H
