/*
 * Copyright (C) 2015 Ludovic Schreiber, Emmanuel Durand
 *
 * This file is part of posturevision.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @solidify.h
 * The SolidifyGPU class, and utility classes
 */

#ifndef __POSTURE_SOLIDIFYGPU_H__
#define __POSTURE_SOLIDIFYGPU_H__

#include <memory>
#include <utility>
#include <vector>

#include <glm/glm.hpp>

#include <pcl/PolygonMesh.h>
#include <pcl/gpu/surface/tsdf_volume.h>

#include "calibrationreader.h"

namespace posture
{
    class SolidifyGPUImpl;

    /*************/
    //! Solidify class, which converts point clouds to meshes
    class SolidifyGPU
    {
        public:
            /**
             * \brief Constructor
             */
            SolidifyGPU();

            /**
             * \brief Destructor
             */
            ~SolidifyGPU();

            /**
             * \brief Get the resulting mesh
             * \param mesh Reference to the object where the mesh will be written
             * \return Timestamp Reference for the grab (in ms, from epoch)
             */
            unsigned long long getMesh(std::vector<unsigned char>& mesh, bool threaded = false) const;
            unsigned long long getMesh(pcl::PolygonMesh::Ptr& mesh) const;
            unsigned long long getMesh(std::vector<pcl::PolygonMesh::Ptr>& multimesh) const;
            
            /**
             * \brief Set the grid resolution for the reconstruction
             * \param res Grid resolution
             */
            void setGridResolution(float res) const;

            /**
             * \brief Set the grid size for the reconstruction
             * \param size Grid size
             */
            void setGridSize(float size) const;

            /**
             * \brief Set the grid size of the X coordinates for the reconstruction
             * \param size Grid X size
             */
            void setGridSizeX(float size) const;

            /**
             * \brief Set the grid size of the y coordinates for the reconstruction
             * \param size Grid Y size
             */
            void setGridSizeY(float size) const;

            /**
             * \brief Set the grid size of the Z coordinates for the reconstruction
             * \param size Grid Z size
             */
            void setGridSizeZ(float size) const;

            /**
             * \brief Set the number of depth maps using for TSDF calculation
             * \param dpmNbr Number of depth maps
             */
            void setDepthMapNbr (unsigned int dpmNbr) const;

            /**
             * \brief Set an input depth map
             * \param index Which map to set
             * \param depth Depth image data
             * \param width Image width 
             * \param height Image height 
             */
            void setInputDepthMap(int index, std::vector<unsigned short>& depth, int width, int height) const;
            void setInputDepthMap(int index, std::vector<unsigned char>& depth, int width, int height) const;

            /**
             * \brief Activate the compression of the output mesh
             * \param active Compression state
             */
            void setCompressMesh(bool active) const;

            /**
             * \brief Activate the saving of created mesh
             * \param active Saving state
             */
            void setSaveMesh(bool active) const;

            /**
             * \brief Set the path where to find the calibration file
             * \param calibration Calibration parameters for the camera
             */
            void setCalibration(const std::vector<CalibrationParams>& calibration) const;

            /**
             * \brief Set the depth map bilateral filtering parameters
             * \param size Kernel size
             * \param sigmaPos Spatial sigma
             * \param sigmaValue Depth value sigma (in mm)
             */
            void setDepthFiltering(int size, float sigmaPos, float sigmaValue) const;

            /**
             * \brief Set the number of iterations for the hole filling algorithm
             * \param iter Number of iterations
             */
            void setHoleFillingIterations(int iter);

        private:
            std::shared_ptr<SolidifyGPUImpl> _impl {nullptr};
    };

} // end of namespace

#endif // __POSTURE_SOLIDIFYGPU_H__
