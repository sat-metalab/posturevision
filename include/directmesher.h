#ifndef __POSTURE_DIRECTMESHER_H__
#define __POSTURE_DIRECTMESHER_H__

#include <mutex>

#include <pcl/pcl_base.h>
#include <pcl/point_types.h>
#include <pcl/PolygonMesh.h>
#include <pcl/surface/organized_fast_mesh.h>

#include "constants.h"
#include "calibrationreader.h"

using namespace std;

namespace posture
{
    /*************/
    //! DirectMesher class
    class DirectMesher 
    {
        public:
            /**
             * \brief Constructor
             */
            DirectMesher(int pixelSize=5, float angleTolerance=12.5);

            /**
             * \brief Destructor
             */
            ~DirectMesher();

            /**
             * \brief Set the path where to find the calibration file
             * \param calibrations Calibration parameters for the cameras
             */
            void setCalibration(const std::vector<CalibrationParams>& calibrations)
            {
                std::unique_lock<std::mutex> lock(_calibrationMutex);
                _calibrationParams = calibrations;
            }
        
            /**
             * \brief Make a point cloud out of a depth image 
             * \param depth_image the image
             * \param width  image width 
             * \param height  image height
             * \param cameraNo  which camera the image came from
             */
            pcl::PointCloud<pcl::PointXYZ>::Ptr
            convertToXYZPointCloud (const vector<unsigned short>& depth_image, int width, int height, int cameraNo) const;
                    
             /**
             * \brief Make a mesh out of a point cloud using OrganizedFastMesh. Returns a timestamp.
             * \param mesh  pointer to the mesh to write into
             * \param cloud pointer to the input cloud 
             */                   
            unsigned long long getMesh(const pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, pcl::PolygonMesh::Ptr& mesh) ;

        private:
            mutable std::mutex _calibrationMutex;
            std::vector<CalibrationParams> _calibrationParams;
            pcl::OrganizedFastMesh<pcl::PointXYZ> ofm;
    };
} // end of namespace

#endif // __POSTURE_DIRECTMESHER_H__

