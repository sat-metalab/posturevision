/*
 * Copyright (C) 2015 Emmanuel Durand
 *
 * This file is part of posturevision.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/*
 * @coretypes.h
 * A few, mostly basic, types
 */

#include "config.h"

#include <algorithm>
#include <atomic>
#include <chrono>
#include <ostream>
#include <memory>
#include <mutex>
#include <string>
#include <vector>

#ifndef CORETYPES_H
#define CORETYPES_H

namespace posture
{

struct Value;
typedef std::vector<Value> Values;

/*************/
struct Value
{
    public:
        enum Type
        {
            i = 0,
            f,
            s,
            v
        };

        Value() {_i = 0; _type = Type::i;}
        Value(int v) {_i = v; _type = Type::i;}
        Value(float v) {_f = v; _type = Type::f;}
        Value(double v) {_f = (float)v; _type = Type::f;}
        Value(std::string v) {_s = v; _type = Type::s;}
        Value(const char* c) {_s = std::string(c); _type = Type::s;}
        Value(Values v) {_v = v; _type = Type::v;}

        template<class InputIt>
        Value(InputIt first, InputIt last)
        {
            _type = Type::v;
            _v.clear();

            auto it = first;
            while (it != last)
            {
                _v.push_back(Value(*it));
                ++it;
            }
        }

        bool operator==(Value v) const
        {
            if (_type != v._type)
                return false;
            else if (_type == Type::i)
                return _i == v._i;
            else if (_type == Type::f)
                return _f == v._f;
            else if (_type == Type::s)
                return _s == v._s;
            else if (_type == Type::v)
            {
                if (_v.size() != v._v.size())
                    return false;
                bool isEqual = true;
                for (int i = 0; i < _v.size(); ++i)
                    isEqual &= (_v[i] == v._v[i]);
                return isEqual;
            }
            else
                return false;
        }

        int asInt() const
        {
            if (_type == Type::i)
                return _i;
            else if (_type == Type::f)
                return (int)_f;
            else if (_type == Type::s)
                try {return std::stoi(_s);}
                catch (...) {return 0;}
            else
                return 0;
        }

        float asFloat() const
        {
            if (_type == Type::i)
                return (float)_i;
            else if (_type == Type::f)
                return _f;
            else if (_type == Type::s)
                try {return std::stof(_s);}
                catch (...) {return 0.f;}
            else
                return 0.f;
        }

        std::string asString() const
        {
            if (_type == Type::i)
                try {return std::to_string(_i);}
                catch (...) {return std::string();}
            else if (_type == Type::f)
                try {return std::to_string(_f);}
                catch (...) {return std::string();}
            else if (_type == Type::s)
                return _s;
            else
                return "";
        }

        Values asValues() const
        {
            if (_type == Type::i)
                return {_i};
            else if (_type == Type::f)
                return {_f};
            else if (_type == Type::s)
                return {_s};
            else if (_type == Type::v)
                return _v;
            else
                return {};
        }

        void* data()
        {
            if (_type == Type::i)
                return (void*)&_i;
            else if (_type == Type::f)
                return (void*)&_f;
            else if (_type == Type::s)
                return (void*)_s.c_str();
            else
                return nullptr;
        }

        Type getType() const {return _type;}
        
        int size()
        {
            if (_type == Type::i)
                return sizeof(_i);
            else if (_type == Type::f)
                return sizeof(_f);
            else if (_type == Type::s)
                return _s.size();
            else
                return 0;
        }

    private:
        Type _type;
        int _i {0};
        float _f {0.f};
        std::string _s {""};
        Values _v {};
};

/*************/
// OnScopeExit, taken from Switcher (https://github.com/nicobou/switcher)
template <typename F>
class ScopeGuard
{
    public:
        explicit ScopeGuard(F &&f) :
            f_(std::move(f)) {}
        ~ScopeGuard()
        {
            f_();
        }
    private:
        F f_;
};

enum class ScopeGuardOnExit { };
template <typename F>
ScopeGuard<F> operator+(ScopeGuardOnExit, F&& f)
{
    return ScopeGuard<F>(std::forward<F>(f));
}

#define CONCATENATE_IMPL(s1, s2) s1##s2
#define CONCATENATE(s1, s2) CONCATENATE_IMPL(s1, s2)
#define OnScopeExit auto CONCATENATE(on_scope_exit_var, __LINE__) = ScopeGuardOnExit() + [&]()

} // end of namespace

#endif // CORETYPES_H
