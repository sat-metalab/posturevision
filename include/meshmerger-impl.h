/*
 * Copyright (C) 2013 Emmanuel Durand, Bruno Roy
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @pointcloudmerger.h
 * The MeshMergerImpl class
 */

#ifndef __MESHMERGER_IMPL_H__
#define __MESHMERGER_IMPL_H__

#include <functional>
#include <iostream>
#include <string>
#include <memory>
#include <mutex>
#include <vector>

#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/TextureMesh.h>
#include <pcl/PolygonMesh.h>
#include <pcl/registration/transforms.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/passthrough.h>
#include <pcl/point_cloud.h>

#include "calibrationreader.h"
#include "config.h"
#include "constants.h"
#include "calibrationreader.h"
#include "filter.h"
#include "meshserializer.h"

namespace posture
{
    /*************/
    //! MeshMergerImpl class, merges meshes according to a calibration file
    class MeshMergerImpl
    {
        public:
            /**
             * \brief Constructor
             * \param meshNbr Number of input meshes to be merged
             */
            MeshMergerImpl(const unsigned int meshNbr = 1);
    
            /**
             * \brief Destructor
             */
            ~MeshMergerImpl();

            /**
             * \brief Set whether to apply the calibration (including clipping) or not
             * \param apply true to apply calibration (including clipping)
             */
            void setApplyCalibration(bool apply) {_doApplyCalibration = apply;}

            /**
             * \brief Set the path where to find the calibration file
             * \param calibration Calibration parameters for the camera
             */
            void setCalibration(const std::vector<CalibrationParams>& calibrations)
            {
                std::unique_lock<std::mutex> lock(_mutex);
                _calibrationParams = calibrations;
                initializeTransformation();
            }
    
            /**
             * \brief Set one of the input meshes
             * \param index Index of the mesh to be set
             * \param mesh Added mesh
             */
            void setInputMesh(const unsigned int index, const std::vector<unsigned char>& mesh);
            void setInputMesh(const unsigned int index, std::vector<unsigned char>&& mesh);
            void setInputMesh(const unsigned int index, const pcl::PolygonMesh::Ptr mesh);
            void setInputMesh(const unsigned int index, const pcl::TextureMesh::Ptr mesh);
    
            /**
             * \brief Get the merged mesh
             * \param mesh Pointer on the structure where to write the output point mesh
             * \return Timestamp for the grab (in ms, from epoch)
             */
            unsigned long long getMesh(std::vector<unsigned char>& mesh);
            unsigned long long getMesh(pcl::TextureMesh::Ptr mesh);
            unsigned long long getMesh(pcl::PolygonMesh::Ptr mesh);
    
            /**
             * \brief Starts the merger
             */
            void start();
    
            /**
             * \brief Stops the merger
             */
            void stop();
    
        private:
            MeshSerializer _serializer;
            Filter _filter;
    
            pcl::TextureMesh::Ptr _mesh;
            std::vector<std::vector<unsigned char>> _rawMeshes;
            std::vector<pcl::TextureMesh::Ptr> _meshes;
    
            std::vector<CalibrationParams> _calibrationParams;
            std::vector<Eigen::Matrix4f> _matTransforms;
            bool _doApplyCalibration {true};

            std::vector<unsigned long long> _timestamps;
            unsigned long long _mergedTimestamp {0};
    
            unsigned int _nbrSources;
            bool _saveSeparately;
    
            mutable std::mutex _mutex;
            mutable std::mutex _inputMutex;
    
            // Methods
            void initializeTransformation();
            void updateCalibration();
    };

} // end of namespace

#endif // MESHMERGER_H
