/*
 * Copyright (C) 2013 Emmanuel Durand, Bruno Roy
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @pointcloudmerger.h
 * The PointCloudMerger class
 */

#ifndef __POSTURE_POINTCLOUDMERGER_H__
#define __POSTURE_POINTCLOUDMERGER_H__

#include <functional>
#include <iostream>
#include <string>
#include <memory>
#include <utility>
#include <mutex>
#include <vector>

#include <glm/glm.hpp>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#include "calibrationreader.h"

namespace posture
{
    class PointCloudMergerImpl;

    /*************/
    //! PointCloudMerger class, merges clouds according to a calibration file
    class PointCloudMerger
    {
        public:
            /**
             * \brief Constructor
             */
            PointCloudMerger();
    
            /**
             * \brief Destructor
             */
            ~PointCloudMerger();
    
            /**
             * \brief Set the path where to find the calibration file
             * \param calibration Calibration parameters for the camera
             */
            void setCalibration(const std::vector<CalibrationParams>& calibrations) const;
            
            /**
             * \brief Set the number of clouds to merge
             * \param cloudNbr Number of clouds
             */
            void setCloudNbr (unsigned int cloudNbr) const;

            /**
             * \brief Set whether to compress the output cloud or not
             * \param compress true for compress the output cloud
             */
            void setCompression(bool compress) const;

            /**
             * \brief Set the downsampling
             * \param active Downsampling is active if true
             * \param resolution Target resolution
             */
            void setDownsampling(bool active, float resolution) const;

            /**
             * \brief Sets the size of the random noise (used if no input cloud is set).
             * \param size Size of the cloud. The effective value used is the square of this value
             */
            void setRandomNoiseSize(const unsigned int size) const;
    
            /**
             * \brief Set one of the input clouds
             * \param index Index of the cloud to be set
             * \param cloud added point cloud
             * \param compressed true if the input cloud is compressed
             */
            void setInputCloud(const unsigned int index, const std::vector<char>& cloud, bool compressed) const;
            void setInputCloud(const unsigned int index, const pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud) const;

            /**
             * \brief Activate the saving of current cloud
             * \param active Saving state
             */
            void setSaveCloud(bool active) const;
    
            /**
             * \brief Get the merged cloud
             * \param cloud pointer on the object where to write the cloud
             * \return Timestamp for the grab (in ms, from epoch)
             */
            unsigned long long getCloud(std::vector<char>& cloud) const;
            unsigned long long getCloud(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud) const;
    
            /**
             * \brief Sets if a cloud is loaded from file.
             * \param pcdPath Path to PCD file.
             */
            void setLoadedPCD(std::string pcdPath) const;

            /**
             * \brief Set whether to save the clouds separately or not
             * \param saveSeparately true to save the inputs in separate files
             */
            void setSaveSeparately (const bool saveSeparately) const;
    
            /**
             * \brief Starts the merger
             */
            void start() const;
    
            /**
             * \brief Stops the merger
             */
            void stop() const;
    
        private:
            std::shared_ptr<PointCloudMergerImpl> _impl {nullptr};
    };

} // end of namespace

#endif // POINTCLOUDMERGER_H
