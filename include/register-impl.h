#ifndef __POSTURE_REGISTER_IMPL_H__
#define __POSTURE_REGISTER_IMPL_H__

#include <memory>
#include <mutex>
#include <utility>
#include <vector>

#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/icp_nl.h>

#include "calibrationreader.h"
#include "config.h"
#include "constants.h"
#include "pointcloudserializer.h"

namespace posture
{
    /*************/
    //! Register class, which converts point clouds to meshes
    class RegisterImpl
    {
        public:
            /**
             * \brief Constructor
             */
            RegisterImpl();

            /**
             * \brief Destructor
             */
            ~RegisterImpl();

            /**
             * \brief Get the updated calibrations
             */
            std::vector<CalibrationParams> getCalibration();
    
            /**
             * \brief Set one of the input clouds
             * \param index Index of the cloud to be set
             * \param cloud added point cloud
             * \param compressed true if the input cloud is compressed
             */
            void setInputCloud(const unsigned int index, const std::vector<char>& cloud, bool compressed);
            void setInputCloud(const unsigned int index, const pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud);

            /**
             * \brief Set the path where to find the calibration file
             * \param calibration Guess calibration parameters for the cameras
             */
            void setGuessCalibration(const std::vector<CalibrationParams>& calibration);

        private:
            std::mutex _mutex {};

            PointCloudSerializer<pcl::PointXYZRGBNormal> _serializer {};
    
            std::vector<unsigned long long> _timestamps;
            std::vector<pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr> _pointClouds {};
            std::vector<CalibrationParams> _guessedCalibrationParams {};
            std::vector<CalibrationParams> _calibrationParams {};

            pcl::IterativeClosestPointNonLinear<pcl::PointXYZRGBNormal, pcl::PointXYZRGBNormal> _icp {};
    };

} // end of namespace

#endif // __POSTURE_REGISTER_IMPL_H__
