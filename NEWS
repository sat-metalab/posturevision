Posturevision release notes
============================

2013-06-21 Kinectvision 0.4.0
-----------------------------
This version is an official release in the 0.4 stable series. 
It contains important changes in the architecture and usage, with a lot of 
improvement (not all described here)

New features:
* Added posture-camera: captures point clouds, depth, rgb and ir grabs from a
  single camera, and writes it to a shmdata
* Added posture-merge: reads clouds from multiple shmdatas and merges them
  according to the configuration file
* Added posture-play: replays point clouds sequences recording when a specific
  option is set in posture-merge
* Updated posture-calibrate to support this new architecture: calibration is 
  now done by reading shmdatas
* Added posture-detect: simple cluster detection in a point clouds
* Added wrappers subproject, containing currently a Python wrapper (optionally
  build)
* Support for Asus XTion added

Bug fixes:

* Iframe compression is now possible over the network
* No more rgb buffer switching between cameras
* A lot more of updates, correction, and documentation...


2012-10-16 Kinectvision 0.2.0
-----------------------------
This version is an official release in the 0.2 stable series. 
It contains bug fixes and new features.

New features:

* Posture-capture: support new calibration system (with rotation vectors)
* Add posture-calibrate calibration system
* Message dialog to confirm replacement of calibration file for Posture-capture

Bug fixes:

* Remove dependency to libfreenect 
* Remove second passthrough filter. (Used to cause a segmentation fault)
* Delete shared memory file at startup
* Fix posture-workstation
* Updates and tests on Posture-Vision
* Remove unused includes in project.
* Correct mutex issue has been corrected (no more segmentation faults during transformations)


2012-05-17 Kinectvision 0.1.6
-----------------------------
This release is a snapshot in the 0.1 unstable series. 
This version contains new features and bug fixes.

New features:
    * Edition des nuages de points
    * Visual feedback (progress bar & scripts) when saving files to disk.
    * SIFT registration and normals resampling.
    * Module de capture et interface de communication entre les modules
    * Acquisition de nuage de points à l'aide de l'interface utilisateur.
    * sauvegarde d'un nuage de point (fusion + compression)
    * Calibration des caméras et sauvegarde/chargement d'un fichier de calibration
    * Avancement prototype visualisation
    * add kinectvision-qt-pcl vizualisation prototype
    * Filtre pour nettoyer le nuage de points et fusion de deux nuages de points.
    * Correction des couleurs RGB à la fusion des images
    * Avancements des prototypes Procedural et Transformation
    * Add compression prototype
    * add transformation prototype
    * add procedural prototype
    * add prototypes/pcl-openniviewer
	* Semi-supervised calibration
	* Synchronize camera callback functions
	* Possibility to generate random noise for testing purpose
	* Command line version of Posture-capture
	* Capture point clouds
	* Editing point clouds (crop, transform and color properties)

Bug fixes:
    * Fixes in options parsing
    * Correction et progression sur effet de latence
	* Fixes memory leaks with shared pointers


2011-09-27 Kinectvision 0.1.4
-----------------------------
This release is a snapshot in the 0.1 unstable series. 
This version contains bug fixes.

Bug fixes:
    * Fixes in options parsing


2011-09-27 Kinectvision 0.1.2
-----------------------------
This release is a snapshot in the 0.1 unstable series. 
This version contains new features.

New features:
    * Load settings at startup if desired
    * Interactive rotation
    * Rotated point clouds so that cameras are in portrait, their right side down


2011-08-18 Kinectvision 0.1.0
-----------------------------
This release is a snapshot in the 0.1 unstable series. 
Initial working prototype.

