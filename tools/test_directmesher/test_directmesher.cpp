#include <chrono>
#include <iostream>
#include <memory>
#include <thread>
#include <vector>
#include <random>

#include <boost/make_shared.hpp>

#include <pcl/io/ply_io.h>

#include "calibrationreader.h"
#include "meshmerger.h"
#include "display.h"
#include "filter.h"
#include "directmesher.h"
#include "meshserializer.h"

#include <shmdata/console-logger.hpp>
#include <shmdata/follower.hpp>


using namespace std;

/*************/
// A shmdata logger
class ConsoleLogger: public shmdata::AbstractLogger
{
    private:
        void on_error(std::string &&str) final
        {
            cout << "Shmdata::ConsoleLogger - " << str << endl;
        }
        void on_critical(std::string &&str) final
        {
            cout << "Shmdata::ConsoleLogger - " << str << endl;
        }
        void on_warning(std::string &&str) final
        {
            cout << "Shmdata::ConsoleLogger - " << str << endl;
        }
        void on_message(std::string &&str) final
        {
            cout << "Shmdata::ConsoleLogger - " << str << endl;
        }
        void on_info(std::string &&str) final
        {
            cout << "Shmdata::ConsoleLogger - " << str << endl;
        }
        void on_debug(std::string &&str) final
        {
            cout << "Shmdata::ConsoleLogger - " << str << endl;
        }
};

template <typename T>
std::vector<T> operator+(const std::vector<T>& a, const std::vector<T>& b)
{
    assert(a.size() == b.size());

    std::vector<T> result;
    result.reserve(a.size());

    std::transform(a.begin(), a.end(), b.begin(), 
                   std::back_inserter(result), std::plus<T>());
    return result;
}

// connects to a set of 3 camera recording shmdatas in /tmp/ and displays them using OrganizedFastMesh and MeshMerger.
// calibration data should be in file default.kvc

int main(int argc, char* argv[])
{ 
    // something to output any errors, warnings, etc.
    ConsoleLogger logger;

    // we're playing with data from 3 cameras so we need to hold 3 Followers
    vector<unique_ptr<shmdata::Follower>> readers;

    ///////// mesh reconstruction & display setup
    std::mutex meshMutex;

    // parameters
    int pixelResolution=4; // spacing between sampled points in depth image
    if (argc>1) pixelResolution = atoi(argv[1]);
    float angleTolerance=12.5; // for OrganizedFastMesh
    if (argc>2) angleTolerance = atof(argv[2]);
    int clippingD = 65000; // direct clipping of image data
    if (argc>3) clippingD = atoi(argv[3]);

    // we need a DirectMesher object to handle incoming images and turn them into meshes
    posture::DirectMesher _directMesher(pixelResolution, angleTolerance); // 4-pixel spacing

    // to hold the meshes
    auto mesh  = boost::make_shared<pcl::PolygonMesh>();
    auto mesh0 = boost::make_shared<pcl::PolygonMesh>();
    auto mesh1 = boost::make_shared<pcl::PolygonMesh>();
    auto mesh2 = boost::make_shared<pcl::PolygonMesh>();

    // to display the mesh
    posture::Display _display;
    _display.setWireframe(false);

    // to filter
    posture::Filter filter;

    ///////// calibration setup
    std::unique_ptr<posture::CalibrationReader> _calibrationReader = unique_ptr<posture::CalibrationReader>(new posture::CalibrationReader("default.kvc"));
    auto calibrations = _calibrationReader->getCalibrationParams();

    _directMesher.setCalibration(calibrations);

    // mesh merger setup
    
    posture::MeshMerger mm(3); // 3 meshes to merge
    mm.setApplyCalibration(true);
    mm.setCalibration(calibrations);

    int sample_n = 0;

    /////////// temporal filtering/
    vector<vector<unsigned short>> depthMapMemory;
    for (int i = 0; i < 3; ++i)  // set up the initial memories
    {
        vector<unsigned short> img(640*480);
        depthMapMemory.push_back(img);
    }
    
    vector<double> reconTimes;
    auto timeAtStart = chrono::high_resolution_clock::now();
    int  timeAtStart_ms = chrono::duration_cast<chrono::milliseconds>(timeAtStart.time_since_epoch()).count();

    for (int i = 0; i < 3; ++i)  // set up the Followers
    {
        // each Follower needs to be given 3 functions: (see reader.hpp)
        // 1) what to do upon receiving data
        // 2) what to do upon server connection
        // 3) what to do upon server disconnection
        // AND a logger to receive any errors or warnings
        readers.push_back(unique_ptr<shmdata::Follower>(new shmdata::Follower("/tmp/camera_" + to_string(i),
            [&, i](void* data, size_t size) { // 1) feed buffer to mesher
                std::unique_lock<std::mutex> lock(meshMutex);
                auto timeRightNow = chrono::high_resolution_clock::now();
                int  timeRightNow_ms = chrono::duration_cast<chrono::milliseconds>(timeRightNow.time_since_epoch()).count();

                cout << endl << 1000*(++sample_n)/(timeRightNow_ms-timeAtStart_ms) << " submeshes per second" << endl;

                cout << endl << sample_n << ". cam #" << i << " buffer size " << size << " bytes // " << endl;
                unsigned short * uShortBuf = (unsigned short *)data; // 16-bit depth
                int bufSizeInUnsignedShort = size/2;
                vector<unsigned short> depthMap(uShortBuf, uShortBuf + bufSizeInUnsignedShort);

                cout << "PLAY:" << "depthMap size " << depthMap.size() << " depthMapMemory[i] size " << depthMapMemory[i].size() << endl;
                
                vector<unsigned short> smoothedDepthMap(bufSizeInUnsignedShort);
                for (int k=0; k<depthMap.size(); ++k)
                {
                    smoothedDepthMap[k] = depthMapMemory[i][k]/2 + depthMap[k]/2;
                    depthMapMemory[i][k] = smoothedDepthMap[k];
                    // simple clipping
                    if(smoothedDepthMap[k] > clippingD) smoothedDepthMap[k]=0;
                }

                /*
                // simple clipping
                for (int k=0; k<depthMap.size(); ++k)
                {
                    if(depthMap[k]>clippingD) depthMap[k]=0;
                }
                */

                if (true)
                {
                    // log the time
                    auto timenow = chrono::high_resolution_clock::now();
                    int  time_ms = chrono::duration_cast<chrono::milliseconds>(timenow.time_since_epoch()).count();
                    // make a point cloud from the depth map
                    auto nuage = _directMesher.convertToXYZPointCloud(smoothedDepthMap, 640, 480, i);

                    // generate a direct mesh out of the point cloud
                    cout << endl << "PLAY: generating a mesh" << endl;
                    auto targetMesh = (i==0 ? mesh0 : (i==1? mesh1 : mesh2));
                    _directMesher.getMesh(nuage, targetMesh); 

                    int nbrPolys, nbrVertices, serSize;
                    cout << "PLAY: # polygons in mesh: " << (nbrPolys = targetMesh->polygons.size()) << " // ";
                    cout << "PLAY: # vertices in mesh: " << (nbrVertices = targetMesh->cloud.width * targetMesh->cloud.height) << endl;

                    // feed this to the mesh merger
                    mm.setInputMesh(i,targetMesh);

                    // report the time
                    timenow = chrono::high_resolution_clock::now();
                    int time_ms1 = chrono::duration_cast<chrono::milliseconds>(timenow.time_since_epoch()).count();
                    cout << "PLAY: time elapsed during direct mesh creation is >>>> " << time_ms1-time_ms << " ms <<<< " << endl << endl;

                    // merge meshes
                    mm.getMesh(mesh);
                    cout << "PLAY: # polygons in merged mesh: " << (nbrPolys = mesh->polygons.size()) << " // ";
                    cout << "PLAY: # vertices in merged mesh: " << (nbrVertices = mesh->cloud.width * mesh->cloud.height) << endl;

                    // report the time
                    timenow = chrono::high_resolution_clock::now();
                    int time_ms2 = chrono::duration_cast<chrono::milliseconds>(timenow.time_since_epoch()).count();
                    cout << "PLAY: time elapsed during mesh merger is >>>> " << time_ms2-time_ms1 << " ms <<<< " << endl << endl;

                    // now serialize the mesh
                    posture::MeshSerializer meshSerializer;
                    meshSerializer.setCompressionMethod(CTM_METHOD_MG1, 1, 0.01);
                    std::vector<unsigned char> serialization = meshSerializer.serialize(mesh, 0);

                    // report the time
                    timenow = chrono::high_resolution_clock::now();
                    int time_ms3 = chrono::duration_cast<chrono::milliseconds>(timenow.time_since_epoch()).count();
                    cout << "PLAY: time elapsed during mesh serialization is >>>> " << time_ms3-time_ms2 << " ms <<<< " << endl << endl;
                    cout << "PLAY: serialization size (bytes): " << serialization.size() << " " << serialization.size()/float(nbrPolys) << " Bpp" << endl;

                    cout << " = = = = = = = PLAY: TOTAL TIME >>>> " << time_ms3-time_ms << " ms <<<< ";
                    cout << "at a resolution of " << pixelResolution << " pixels - # polygons: " << nbrPolys << endl << endl << endl;
                    reconTimes.push_back(time_ms3-time_ms);

                    double sum = std::accumulate(reconTimes.begin(), reconTimes.end(), 0.0);
                    cout << "average time " << sum / reconTimes.size() << endl;
                }

            }, // 2) caps specifies what kind of data is in the buffer
            [=](const string& caps) {
                cout << "cam #" << i << " caps ===> " << caps << endl;
            },  // 3) do nothing
            [&](){exit(0);},
            &logger)));
    }
    
    // then the Followers are set; they will keep an eye out and react when data comes in. Voila!

    bool doDisplay = true;
    while (doDisplay)
    {
        this_thread::sleep_for(std::chrono::milliseconds(33));

        std::unique_lock<std::mutex> lock(meshMutex); //make sure nobody's writing into the mesh
        _display.setPolygonMesh(mesh);
    }

    return 0;

}
