/*
 * Copyright (C) 2014 Emmanuel Durand, Bruno Roy
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @display.cpp
 * Point cloud display tool
 */

#include <chrono>
#include <iostream>
#include <memory>
#include <mutex>
#include <string>
#include <thread>

#include <shmdata/console-logger.hpp>
#include <shmdata/follower.hpp>

#include "display.h"

using namespace std;
using namespace posture;

/*************/
// A shmdata logger
class ConsoleLogger: public shmdata::AbstractLogger
{
    private:
        void on_error(std::string &&str) final
        {
            cout << "Shmdata::ConsoleLogger - " << str << endl;
        }
        void on_critical(std::string &&str) final
        {
            cout << "Shmdata::ConsoleLogger - " << str << endl;
        }
        void on_warning(std::string &&str) final
        {
            cout << "Shmdata::ConsoleLogger - " << str << endl;
        }
        void on_message(std::string &&str) final
        {
            cout << "Shmdata::ConsoleLogger - " << str << endl;
        }
        void on_info(std::string &&str) final
        {
            cout << "Shmdata::ConsoleLogger - " << str << endl;
        }
        void on_debug(std::string &&str) final
        {
            cout << "Shmdata::ConsoleLogger - " << str << endl;
        }
};

/*************/
string _socketPath {};
bool _verbose;

shared_ptr<Display> _display {nullptr};
ConsoleLogger _logger;
std::unique_ptr<shmdata::Follower> _reader {nullptr};
std::string _caps {};
mutex _displayMutex {};

#define POINTCLOUD_TYPE_BASE          "application/x-pcl"
#define POINTCLOUD_TYPE_COMPRESSED    "application/x-pcd"
#define POLYGONMESH_TYPE_BASE         "application/x-polymesh"

/*************/
void parseArgs(int argc, char** argv)
{
    int idx = 0;
    while (idx < argc)
    {
        if ((string(argv[idx]) == "-s" || string(argv[idx]) == "--socket-path") && idx + 1 < argc)
        {
            _socketPath = string(argv[idx + 1]);
            idx += 2;
        }
        else if (string(argv[idx]) == "-v" || string(argv[idx]) == "--verbose")
        {
            _verbose = true;
            idx++;
        }
        else if (string(argv[idx]) == "-h" || string(argv[idx]) == "--help")
        {
            cout << "Point cloud display" << endl;
            cout << "Options:" << endl;
            cout << "\t-s (--socket-path) [filename] : set [filename] as the socket path to read from" << endl;
            cout << "\t-v (--verbose) : be more verbose" << endl;
            cout << "\t-h (--help) : show this very message" << endl;
            exit(0);
        }
        else
            idx++;
    }

    if (_socketPath == "")
        exit(0);
}

/*************/
void onData(void* data, int data_size, void* user_data)
{
    if (_display == nullptr)
        return;
    
    //lock_guard<mutex> lock(_displayMutex);
    if (!_displayMutex.try_lock())
        return;

    if (_caps == string(POINTCLOUD_TYPE_COMPRESSED) || _caps == string(POINTCLOUD_TYPE_BASE))
    {
        vector<char> buffer((char*)data, (char*)data + data_size);
        _display->setInputCloud(buffer, _caps == string(POINTCLOUD_TYPE_COMPRESSED));
    }
    else if (_caps == string(POLYGONMESH_TYPE_BASE))
    {
        vector<unsigned char> buffer((unsigned char*)data, (unsigned char*)data + data_size);
        _display->setPolygonMesh(buffer);
    }
                           

   _displayMutex.unlock();
}

/*************/
void onCaps(const string& caps, void*)
{
    unique_lock<mutex> lock(_displayMutex);
    _caps = caps;
}

/*************/
int main(int argc, char** argv)
{
    parseArgs(argc, argv);

    _display = make_shared<Display>(_socketPath);

    _reader.reset(new shmdata::Follower(_socketPath,
                                        [&](void* data, size_t size) {
                                            onData(data, size, nullptr);
                                        },
                                        [&](const string& caps) {
                                            onCaps(caps, nullptr);
                                        },
                                        [&](){},
                                        &_logger));

    while (!_display->wasStopped())
    {
        this_thread::sleep_for(chrono::milliseconds(16));
    }

    unique_lock<mutex> lock(_displayMutex);

    return 0;
}
