#ifndef TEST_MC_H
#define TEST_MC_H

#include <iostream>
#include <mutex>
#include <vector>
#include <Eigen/Core>

#include <boost/make_shared.hpp>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/PolygonMesh.h>
#include <pcl/conversions.h>
#include <pcl/gpu/surface/tsdf_volume.h>
#include <pcl/gpu/surface/marching_cubes.h>
#include <pcl/gpu/containers/device_array.h>

#include <calibrationreader.h>
#include <colorize.h>
#include <colorizeGL.h>
#include <detect.h>
#include <display.h>
#include <meshmerger.h>
#include <pointcloudmerger.h>
#include <solidify.h>
#include <zcamera.h>
#include <solidifyGPU.h>

void DeviceArrayToPolygonMesh (const pcl::gpu::DeviceArray<pcl::PointXYZ>& Array, pcl::PolygonMesh::Ptr Mesh);

class MC_GPU
{
public:
    MC_GPU ();
    ~MC_GPU ();
    void run ();

private:
    std::unique_ptr<posture::CalibrationReader> _calibrationReader;

    void CloudCallback1 (pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud);
    void DepthCallback1 (std::vector<unsigned char>& depth, int width, int height);
    void CloudCallback2 (pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud);
    void DepthCallback2 (std::vector<unsigned char>& depth, int width, int height);
    void CloudCallback3 (pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud);
    void DepthCallback3 (std::vector<unsigned char>& depth, int width, int height);

    Eigen::Vector3i _resolution;

    posture::ZCamera _cam1;
    posture::ZCamera _cam2;
    posture::ZCamera _cam3;
    posture::Display _disp;
    posture::PointCloudMerger _merge;
    posture::SolidifyGPU _mesh_creator;

    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr _cloud;

    std::mutex _cloudMutex;
    std::mutex _depthMutex;

    pcl::gpu::DeviceArray<pcl::PointXYZ> _triangle_buffer;
};

#endif // TEST_MC_H
