#include "./test_colorizeGL.h"

#include <chrono>
#include <thread>
#include <signal.h>

#include <pcl/io/obj_io.h>

#define POINTCLOUD_TYPE_BASE          "application/x-pcl"
#define POINTCLOUD_TYPE_COMPRESSED    "application/x-pcd"
#define POLYGONMESH_TYPE_BASE         "application/x-polymesh"

using namespace std;
using namespace posture;

/*************/
// A shmdata logger
class ConsoleLogger: public shmdata::AbstractLogger
{
    private:
        void on_error(std::string &&str) final
        {
            cout << "Shmdata::ConsoleLogger - " << str << endl;
        }
        void on_critical(std::string &&str) final
        {
            cout << "Shmdata::ConsoleLogger - " << str << endl;
        }
        void on_warning(std::string &&str) final
        {
            cout << "Shmdata::ConsoleLogger - " << str << endl;
        }
        void on_message(std::string &&str) final
        {
            cout << "Shmdata::ConsoleLogger - " << str << endl;
        }
        void on_info(std::string &&str) final
        {
            cout << "Shmdata::ConsoleLogger - " << str << endl;
        }
        void on_debug(std::string &&str) final
        {
            cout << "Shmdata::ConsoleLogger - " << str << endl;
        }
};

/*************/
App::App()
{
    _cameras.resize(CAMERA_NBR);

    _calibrationReader = unique_ptr<posture::CalibrationReader>(new posture::CalibrationReader("default.kvc"));
    auto calibrations = _calibrationReader->getCalibrationParams();

    _register = unique_ptr<posture::Register>(new posture::Register());

    for (int i = 0; i < CAMERA_NBR; ++i)
    {
        _cameras[i] = unique_ptr<posture::ZCamera>(new posture::ZCamera);
        //_cameras[i]->setCaptureMode(ZCamera::CaptureMode_Default_Mode);
        //_cameras[i]->setDownsampling(true, 0.06);

        _cameras[i]->setCalibration(calibrations[i]);
        _cameras[i]->setDeviceIndex(i);

        _cameras[i]->setCallbackDepth([=](void*, std::vector<unsigned char>& depth, int width, int height) {
            callbackDepth(i, depth, width, height);
        }, nullptr);

        _cameras[i]->setCallbackRgb([=](void*, std::vector<unsigned char>& rgb, int width, int height) {
            callbackRGB(i, rgb, width, height);
        }, nullptr);

        _cameras[i]->setCallbackCloud([=](void*, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud) {
            callbackCloud(i, cloud);
        }, nullptr);
    }

    _mesher.setGridSize(4.0);
    _mesher.setGridResolution(4.0 / 128.0);
    _mesher.setCalibration(calibrations);
    _mesher.setDepthMapNbr(CAMERA_NBR);

    _colorizer.setCalibration(calibrations);

    _camerasUpdated.resize(CAMERA_NBR, false);
    _images.resize(CAMERA_NBR);
    _dims.resize(CAMERA_NBR);
}

/*************/
App::~App()
{
    if (_registeringThread.joinable())
        _registeringThread.join();

    _cameras.clear();
}

/*************/
void App::run()
{
    Eigen::IOFormat printFormat(4, 0, ", ", "\n", "[", "]");

    for (auto& camera : _cameras)
        if (camera)
        {
            camera->start();
            if (!camera->isReady())
                return;
        }

    pcl::PolygonMesh::Ptr mesh = boost::make_shared<pcl::PolygonMesh>();
    std::vector<uint8_t> meshSerialized {};
    ConsoleLogger logger;

    auto loop = thread([&]() {
    while (_continue)
    {
        _mesher.getMesh(mesh);

        unique_lock<mutex> lockCamera(_cameraMutex);
        _updateCondition.wait(lockCamera);

        if (!_continue)
            break;

        if (!_isRegistering)
        {
            if (_registeringThread.joinable())
                _registeringThread.join();

            _isRegistering = true;
            _registeringThread = thread([=]() {
                _calibrationReader->reload();
                auto calibration = _calibrationReader->getCalibrationParams();

                cout << "-----------------------------------" << endl;
                for (auto& c : calibration)
                {
                    cout << c.getTransformation().format(printFormat) << endl;
                    cout << endl;
                }

                _register->setGuessCalibration(calibration);
                calibration = _register->getCalibration();
                _colorizer.setCalibration(calibration);
                _mesher.setCalibration(calibration);
                _isRegistering = false;

                cout << "+++++++++" << endl;
                for (auto& c : calibration)
                {
                    cout << c.getTransformation().format(printFormat) << endl;
                    cout << endl;
                }
            });
        }

        _colorizer.setInput(mesh, _images, _dims);
        _colorizer.getTexturedMesh(meshSerialized);

        uint32_t width, height;
        vector<uint8_t> img = _colorizer.getTexture(width, height);

        if (meshSerialized.size() > 0)
        {
            if (!_meshWriter || meshSerialized.size() > _meshWriter->alloc_size())
            {
                _meshWriter.reset();
                _meshWriter = make_shared<shmdata::Writer>("/tmp/texturedMesh",
                                                           meshSerialized.size() * 2,
                                                           POLYGONMESH_TYPE_BASE,
                                                           &logger);
            }

            if (_meshWriter)
                _meshWriter->copy_to_shm(meshSerialized.data(), meshSerialized.size());

            if (!_imgWriter || img.size() > _imgWriter->alloc_size())
            {
                _imgWriter.reset();
                _imgWriter = make_shared<shmdata::Writer>("/tmp/texturedImage",
                                                           img.size() * 2,
                                                           "video/x-raw,format=(string)BGR,width=(int)" + to_string(width) + ",height=(int)" + to_string(height) + ",framerate=30/1",
                                                           &logger);
            }

            if (_imgWriter)
                _imgWriter->copy_to_shm(img.data(), img.size());
        }
    }
    });

    if (loop.joinable())
        loop.join();

    cout << "Exiting" << endl;
    _cameras.clear();
}

/*************/
void App::stop()
{
    _continue = false;
}

/*************/
bool App::all(const vector<bool>& status)
{
  for (auto s : status)
    if (!s)
      return false;

  return true;
}

/*************/
void App::zero(vector<bool>& status)
{
  for (uint32_t i = 0; i < status.size(); ++i)
    status[i] = false;
}

/*************/
void App::callbackDepth(uint32_t camId, vector<unsigned char>& depth, int width, int height)
{
    unique_lock<mutex> lockCamera(_cameraMutex);
    _calibrationReader->loadCalibration("default.kvc");
    auto calibrations = _calibrationReader->getCalibrationParams();

    _cameras[camId]->setCalibration(calibrations[camId]);
    _mesher.setInputDepthMap(camId, depth, width, height);

    bool alreadyUpdated = _camerasUpdated[camId];
    _camerasUpdated[camId] = true;

    if (alreadyUpdated || all(_camerasUpdated))
    {
        zero(_camerasUpdated);
        _colorizer.setCalibration(calibrations);
        _updateCondition.notify_one();
    }
}

/*************/
void App::callbackRGB(uint32_t camId, vector<unsigned char>& rgb, int width, int height)
{
    unique_lock<mutex> lockCamera(_cameraMutex);
    std::swap(_images[camId], rgb);
    _dims[camId] = {(uint32_t)width, (uint32_t)height, 3u};
}

/*************/
void App::callbackCloud(uint32_t camId, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud)
{
    unique_lock<mutex> lockCamera(_cameraMutex);
    _register->setInputCloud(camId, cloud);
}

/*************/
shared_ptr<App> app;

/*************/
void leave(int signal_value)
{
    if (app)
        app->stop();
}

/*************/
int main(void)
{
    struct sigaction signals;
    signals.sa_handler = leave;
    signals.sa_flags = 0;
    sigaction(SIGINT, &signals, NULL);
    sigaction(SIGTERM, &signals, NULL);

    app = make_shared<App>();
    app->run();

    return 0;
}
