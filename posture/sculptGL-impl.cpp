#include "sculptGL-impl.h"

#include <boost/make_shared.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/ext.hpp>

#define SHOW_COMPUTATION_TIME false

using namespace std;
using namespace pcl;

namespace posture
{
    /*********/

    static string SHADER_COMPUTE_PROJECT_PP {R"(
        #version 430 core

        #extension GL_ARB_compute_shader : enable
        #extension GL_ARB_shader_storage_buffer_object : enable

        layout(local_size_x = 8, local_size_y = 8) in;

        layout(std430, binding = 0) buffer meshBuffer
        {
            vec4 vertex[];
        };

        layout(std430, binding = 1) buffer normalBuffer
        {
            vec4 pointNormal[];
        };

        layout(std430, binding = 2) buffer primitiveIdBuffer
        {
            int primitiveId[];
        };

        layout(std430, binding = 3) buffer cloudBuffer
        {
            vec4 point[];
        };

        layout(std430, binding = 4) buffer displacementBuffer
        {
            vec4 displaceVertex[];
        };

        uniform float _pointCount;
        uniform float _maxDistance;

        void main(void)
        {
            for (int i = 0; i < _pointCount; i++)
            {
                int id = primitiveId[i];
                float signedDistance = dot(pointNormal[id], point[id]-vertex[i]);
                if (abs(signedDistance) < _maxDistance)
                    displaceVertex[i] = vec4(vertex[i].xyz + signedDistance * pointNormal[id].xyz, 1.0);
                else
                    displaceVertex[i] = vertex[i];

                //displaceVertex[i] = vec4(vertex[i].xyz + vec3(0.5, 0.0, 0.0), 1.0);
            }
        }
    )"};

    // Shaders for a simple tessellation based on point density
    static string SHADER_VERTEX_TESSELLATE {R"(
        #version 430 core

        layout (location = 0) in vec4 _vertex;

        out VS_OUT
        {
            vec4 vertex;
        } vs_out;

        void main(void)
        {
            vs_out.vertex = _vertex;
        }
    )"};

    static string SHADER_TESS_CTRL_TESSELLATE {R"(
        #version 430 core

        layout(vertices = 3) out;

        in VS_OUT
        {
            vec4 vertex;
        } tcs_in[];

        out TCS_OUT
        {
            vec4 vertex;
        } tcs_out[];

        void main(void)
        {
            // set to 1 if input density equals to zero or lower
            //float patchDensity = max(min(float(ceil(tcs_in[gl_InvocationID].density / 2.0)), 10.0), 1.0);

            int tessLevel = 1;
            if (gl_InvocationID == 0)
            {
                gl_TessLevelInner[0] = tessLevel;//patchDensity;
                gl_TessLevelOuter[0] = tessLevel;//patchDensity;
                gl_TessLevelOuter[1] = tessLevel;//patchDensity;
                gl_TessLevelOuter[2] = tessLevel;//patchDensity;
            }

            tcs_out[gl_InvocationID].vertex = tcs_in[gl_InvocationID].vertex;
        }
    )"};

    static string SHADER_TESS_EVAL_TESSELLATE {R"(
        #version 430 core

        layout (triangles, equal_spacing, cw) in;

        in TCS_OUT
        {
            vec4 vertex;
        } tes_in[];

        out TES_OUT
        {
            vec4 vertex;
        } tes_out;

        vec4 interpolate(vec4 v0, vec4 v1, vec4 v2)
        {
            return vec4(gl_TessCoord.x) * v0 + vec4(gl_TessCoord.y) * v1 + vec4(gl_TessCoord.z) * v2;
        }

        void main(void)
        {
            tes_out.vertex = interpolate(tes_in[0].vertex, tes_in[1].vertex, tes_in[2].vertex);
        }
    )"};

    static string SHADER_GEOMETRY_TESSELLATE {R"(
        #version 430 core

        layout (triangles) in;
        layout (triangle_strip, max_vertices = 3) out;

        in TES_OUT
        {
            vec4 vertex;
        } gs_in[];

        out vec4 outVertex;

        void main(void)
        {
            for (int i = 0; i < 3; i++)
            {
                outVertex = gs_in[i].vertex;
                EmitVertex();
            }
            EndPrimitive();
        }
    )"};

    /*********/
    SculptGLImpl::SculptGLImpl()
    {
        //console::setVerbosityLevel(console::L_ERROR);
        _glEngine = make_shared<GLEngine>();
        //_glEngine->setBufferNumber(2);

        _shaderComputeProjectPP = make_shared<Shader>(_glEngine, Shader::prgCompute);
        _shaderComputeProjectPP->setSource(SHADER_COMPUTE_PROJECT_PP, Shader::compute);
        _shaderComputeProjectPP->compileProgram();

        _shaderTessellate = make_shared<Shader>(_glEngine, Shader::prgFeedback);
        _shaderTessellate->setSource(SHADER_VERTEX_TESSELLATE, Shader::vertex);
        _shaderTessellate->setSource(SHADER_TESS_CTRL_TESSELLATE, Shader::tess_ctrl);
        _shaderTessellate->setSource(SHADER_TESS_EVAL_TESSELLATE, Shader::tess_eval);
        _shaderTessellate->setSource(SHADER_GEOMETRY_TESSELLATE, Shader::geometry);
        _shaderTessellate->compileProgram();

        if (_glEngine)
            _ready = true;
    }

    /*********/
    SculptGLImpl::~SculptGLImpl()
    {
    }

    /*********/
    unsigned long long SculptGLImpl::getMesh(std::vector<unsigned char>& mesh)
    {
        pcl::PolygonMesh::Ptr tempMesh = boost::make_shared<pcl::PolygonMesh>();
        getMesh(tempMesh);

        mesh = _meshSerializer.serialize(tempMesh, _timestamp);
        return _timestamp;
    }

    /*********/
    unsigned long long SculptGLImpl::getMesh(pcl::PolygonMesh::Ptr mesh)
    {
        if (!_mesh)
            return 0;

        lock_guard<mutex> lock(_mutex);

        // Get a coarse mesh as input (from MC step)
        pcl::PointCloud<pcl::PointXYZ> cloud;
        _start = std::chrono::system_clock::now();
        pcl::fromPCLPointCloud2(_mesh->cloud, cloud);
        _end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsedSeconds = _end - _start;
        chrono::duration<double>::rep total = elapsedSeconds.count();

        if (SHOW_COMPUTATION_TIME)
            std::cout << "pcl: " << total << " s.\n";

        std::vector<float> meshBuffer;
        std::vector<float> normalBuffer;
        std::vector<float> tessellatedMeshBuffer;
        std::vector<int> densities;
        std::vector<float> cloudBuffer;
        std::vector<float> displacementBuffer;
        std::vector<int> primitiveIds;

        _start = std::chrono::system_clock::now();
        for (auto& polygon : _mesh->polygons)
        {
            for (auto& vertexId : polygon.vertices)
            {
                meshBuffer.push_back(cloud.points[vertexId].x);
                meshBuffer.push_back(cloud.points[vertexId].y);
                meshBuffer.push_back(cloud.points[vertexId].z);
                meshBuffer.push_back(1.0f);
            }
        }
        _end = std::chrono::system_clock::now();
        elapsedSeconds = _end - _start;
        total += elapsedSeconds.count();
        if (SHOW_COMPUTATION_TIME)
            std::cout << "serialize: " << total << " s.\n";

        //std::clog << "mesh: " << _mesh->polygons.size() * 3 << std::endl;

        /****************************** Polygon tessellation ******************************/
        int tessLevel = 1;
        _glEngine->setTessLevel(tessLevel);

        _start = std::chrono::system_clock::now();
        _glEngine->resetBuffers();
        _glEngine->setBufferNumber(2);
        _glEngine->setBuffer(meshBuffer, 0, 4);
        _glEngine->setBuffer(meshBuffer, 1, 4, GL_TRANSFORM_FEEDBACK_BUFFER);
        _glEngine->setShader(_shaderTessellate);

        _end = std::chrono::system_clock::now();
        elapsedSeconds = _end - _start;
        total += elapsedSeconds.count();
        if (SHOW_COMPUTATION_TIME)
            std::cout << "buffers: " << elapsedSeconds.count() << " s.\n";

        _glEngine->setShader(_shaderTessellate);
        _start = std::chrono::system_clock::now();
        _glEngine->run();
        _end = std::chrono::system_clock::now();
        elapsedSeconds = _end - _start;
        total += elapsedSeconds.count();
        if (SHOW_COMPUTATION_TIME)
            std::cout << "tessellation: " << elapsedSeconds.count() << " s.\n";
        GLuint primitivesCount = _shaderTessellate->getQueryValue();

        uint verticesPerPolygon = 3;// Only for triangles
        uint vertexComponentSize = 4;
        tessellatedMeshBuffer.resize(primitivesCount * verticesPerPolygon * vertexComponentSize);
        uint verticesCount = tessellatedMeshBuffer.size() / 4;

        _start = std::chrono::system_clock::now();
        tessellatedMeshBuffer = _glEngine->getFeedbackBuffer<float>(1);
        _end = std::chrono::system_clock::now();
        elapsedSeconds = _end - _start;
        total += elapsedSeconds.count();
        if (SHOW_COMPUTATION_TIME)
            std::cout << "feedback: " << elapsedSeconds.count() << " s.\n";

        /****************************** Update final mesh ******************************/
        _start = std::chrono::system_clock::now();
        mesh->polygons.clear();
        cloud = pcl::PointCloud<pcl::PointXYZ>();
        cloud.width = verticesCount;
        cloud.height = 1;
        cloud.is_dense = false;
        cloud.reserve(verticesCount);
        for (uint p = 0; p < verticesCount; p += 3)
        {
            pcl::Vertices vertices;
            for (uint v = 0; v < verticesPerPolygon; v++)
            {
                int vertexIndex = (p + v) * 4;

                cloud.points.push_back(pcl::PointXYZ(tessellatedMeshBuffer[vertexIndex],
                                                     tessellatedMeshBuffer[vertexIndex+1],
                                                     tessellatedMeshBuffer[vertexIndex+2]));

                vertices.vertices.push_back(cloud.points.size() - 1);
            }
            mesh->polygons.push_back(vertices);
        }
        _end = std::chrono::system_clock::now();
        elapsedSeconds = _end - _start;
        total += elapsedSeconds.count();
        if (SHOW_COMPUTATION_TIME)
            std::cout << "update: " << elapsedSeconds.count() << " s.\n";

        //std::clog << "tessellate: " << mesh->polygons.size() * 3 << std::endl;

        pcl::toPCLPointCloud2(cloud, mesh->cloud);

        for (int i = 0; i < _cloud->size(); i++)
        {
            cloudBuffer.push_back(_cloud->at(i).x);
            cloudBuffer.push_back(_cloud->at(i).y);
            cloudBuffer.push_back(_cloud->at(i).z);
            cloudBuffer.push_back(1.0f);

            normalBuffer.push_back(_cloud->at(i).normal_x);
            normalBuffer.push_back(_cloud->at(i).normal_y);
            normalBuffer.push_back(_cloud->at(i).normal_z);
            normalBuffer.push_back(1.0f);
        }
        displacementBuffer.resize(tessellatedMeshBuffer.size());

        /**************************** Point-polygon matching ****************************/
        _start = std::chrono::system_clock::now();
        std::vector<pcl::PointXYZ> queriesHost;
        queriesHost.reserve(cloud.size());
        int index = 0;
        for (auto& vertex : cloud.points)
        {
            //std::clog << "q[" << index++ << "]: {" << vertex.x << ", " << vertex.y << ", " << vertex.z << "}" << std::endl;
            queriesHost.push_back(vertex);
        }

        uint pointCount = cloud.size();
        pcl::PointCloud<pcl::PointXYZ> displaceCloud;
        displaceCloud.width = ceil(sqrt(static_cast<double>(pointCount)));
        displaceCloud.height = displaceCloud.width;
        displaceCloud.is_dense = false;
        displaceCloud.reserve(_cloud->size());
        for (auto& point : _cloud->points)
        {
            displaceCloud.push_back(pcl::PointXYZ(point.x, point.y, point.z));
        }

        _octree.reset(new pcl::gpu::Octree());
        pcl::gpu::Octree::PointCloud cloudDevice;
        cloudDevice.upload(displaceCloud.points);
        _octree->setCloud(cloudDevice);
        _octree->build();

        pcl::gpu::Octree::Queries queriesDevice;
        queriesDevice.upload(queriesHost);

        pcl::gpu::NeighborIndices resultDevice(queriesDevice.size(), _maxNeighbors);
        //_octree->radiusSearch(queriesDevice, _pointSeparation, _maxNeighbors, resultDevice);
        _octree->approxNearestSearch(queriesDevice, resultDevice);

        resultDevice.sizes.download(densities);
        resultDevice.data.download(primitiveIds);

        /*for (int i = 0; i < primitiveIds.size(); i ++)
        {
            std::clog << "m[" << i << "]: " << primitiveIds.at(i) << std::endl;
        }*/

        _end = std::chrono::system_clock::now();
        elapsedSeconds = _end - _start;
        total += elapsedSeconds.count();
        if (SHOW_COMPUTATION_TIME)
            std::cout << "matching: " << elapsedSeconds.count() << " s.\n";

        //std::clog << "polygons-before: " << (tessellatedMeshBuffer.size() / 4) << std::endl;
        _glEngine->resetBuffers();
        _glEngine->setBufferNumber(5);
        _glEngine->setBuffer(tessellatedMeshBuffer, 0, 4);
        _glEngine->setBuffer(normalBuffer, 1, 4);
        _glEngine->setBuffer(primitiveIds, 2, 1);
        _glEngine->setBuffer(cloudBuffer, 3, 4);
        _glEngine->setBuffer(displacementBuffer, 4, 4);
        _shaderComputeProjectPP->setUniform("_pointCount", {(float)cloud.size()});
        _shaderComputeProjectPP->setUniform("_maxDistance", {_pointSeparation});
        _shaderComputeProjectPP->setComputeSize(8, 8);
        _glEngine->setShader(_shaderComputeProjectPP);
        _glEngine->run();
        displacementBuffer = _glEngine->getFeedbackBuffer<float>(4);
        verticesCount = displacementBuffer.size() / 4;
        //std::clog << "displace: " << verticesCount << std::endl;

        /*glm::ivec3 localSize;
        glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 0, &localSize.x);
        glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 1, &localSize.y);
        glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 2, &localSize.z);
        std::clog << "localSize: {" << localSize.x << ", " << localSize.y << ", " << localSize.z << "}" << std::endl;
        */

        //std::clog << "polygons-after: " << (displacementBuffer.size() / 4) << std::endl;

        /****************************** Update final mesh ******************************/
        mesh->polygons.clear();
        cloud = pcl::PointCloud<pcl::PointXYZ>();
        cloud.width = verticesCount;
        cloud.height = 1;
        cloud.is_dense = false;
        cloud.reserve(verticesCount);
        for (uint p = 0; p < verticesCount; p += 3)
        {
            pcl::Vertices vertices;
            for (uint v = 0; v < verticesPerPolygon; v++)
            {
                int vertexIndex = (p + v) * 4;

                cloud.points.push_back(pcl::PointXYZ(displacementBuffer[vertexIndex],
                                                     displacementBuffer[vertexIndex+1],
                                                     displacementBuffer[vertexIndex+2]));
                //std::clog << "vertex[" << vertexIndex << "]: {" << displacementBuffer[vertexIndex] << ", " << displacementBuffer[vertexIndex+1] << ", " << displacementBuffer[vertexIndex+2] << "}" << std::endl;

                vertices.vertices.push_back(cloud.points.size() - 1);
            }
            mesh->polygons.push_back(vertices);
        }

        pcl::toPCLPointCloud2(cloud, mesh->cloud);

        return _timestamp;
    }

    /*********/
    glm::mat4 SculptGLImpl::getTransformationForCamera(unsigned int index)
    {
        glm::mat4 translation = glm::translate(glm::mat4(1.f), glm::vec3(-_calibrationParams[index].offset_x + _calibrationParams[index].rgbd_distance,
                                                                         -_calibrationParams[index].offset_y,
                                                                         -_calibrationParams[index].offset_z));

        float angle = M_PI - _calibrationParams[index].rot_x * (M_PI / 180.f);
        glm::mat4 rotationX = glm::rotate(glm::mat4(1.f), angle, glm::vec3(1.f, 0.f, 0.f));

        angle = -_calibrationParams[index].rot_y * (M_PI / 180.f);
        glm::mat4 rotationY = glm::rotate(glm::mat4(1.f), angle, glm::vec3(0.f, 1.f, 0.f));

        angle = -_calibrationParams[index].rot_z * (M_PI / 180.f);
        glm::mat4 rotationZ = glm::rotate(glm::mat4(1.f), angle, glm::vec3(0.f, 0.f, 1.f));

        auto transformation = rotationX * rotationY * rotationZ * translation;

        return transformation;
    }

    /*********/
    void SculptGLImpl::setInputMesh(const vector<unsigned char>& mesh)
    {
        lock_guard<mutex> lock(_mutex);

        PolygonMesh::Ptr inputMesh = _meshSerializer.deserialize(mesh, _timestamp);
        _mesh = inputMesh;
    }

    /*********/
    void SculptGLImpl::setInputMesh(PolygonMesh::Ptr mesh)
    {
        lock_guard<mutex> lock(_mutex);

        _mesh = boost::make_shared<PolygonMesh>();
        _mesh->cloud = mesh->cloud;
        _mesh->polygons = mesh->polygons;
    }

    glm::vec3 SculptGLImpl::getPolygonCentroid(pcl::PointXYZ v1, pcl::PointXYZ v2, pcl::PointXYZ v3)
    {
        glm::vec3 centroid(0.0f);
        centroid += glm::vec3(v1.x, v1.y, v1.z);
        centroid += glm::vec3(v2.x, v2.y, v2.z);
        centroid += glm::vec3(v3.x, v3.y, v3.z);
        centroid /= 3;

        return centroid;
    }

    /*********/
    void SculptGLImpl::setPointSeparation(float separation)
    {
        _pointSeparation = separation;
    }

    /*********/
    void SculptGLImpl::setMaxNeighbors(int maxNeighbors)
    {
        _maxNeighbors = maxNeighbors;
    }

    /*********/
    void SculptGLImpl::setInputCloud(const std::vector<char>& cloud)
    {
        PointCloud<PointXYZRGBNormal>::Ptr inputCloud = _cloudSerializer.deserialize(cloud, false, _timestamp);
        _cloud = inputCloud;
    }

    /*********/
    void SculptGLImpl::setInputCloud(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud)
    {
        lock_guard<mutex> lock(_mutex);

        _cloud = boost::make_shared<PointCloud<pcl::PointXYZRGBNormal>>();
        _cloud = cloud;
    }

} // end of namespace
