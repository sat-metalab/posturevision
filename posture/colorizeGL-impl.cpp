#include "colorizeGL-impl.h"

#include <cassert>

#include <boost/make_shared.hpp>
#include <pcl/io/obj_io.h>
#include <pcl/console/print.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace std;
using namespace pcl;

namespace posture
{
    /*********/
    // Shaders for visibility check
    static string SHADER_VERTEX_VISIBILITY {R"(
        #version 430 core

        layout (location = 0) in vec4 _vertex;
        layout (location = 1) in int _id;
        layout (location = 2) in float _visibility;

        out VS_OUT
        {
            flat vec4 vertex;
            flat int id;
            flat float visibility;
        } vs_out;

        void main(void)
        {
            vs_out.vertex = _vertex;
            vs_out.id = _id;
            vs_out.visibility = _visibility;
        }
    )"};

    static string SHADER_GEOMETRY_VISIBILITY {R"(
        #version 430 core

        in VS_OUT
        {
            flat vec4 vertex;
            flat int id;
            flat float visibility;
        } geom_in[];

        out GEOM_OUT
        {
            flat vec4 vertex;
            flat vec3 normal;
            flat int id;
            flat float visibility;
        } geom_out;

        layout (triangles) in;
        layout (triangle_strip, max_vertices = 3) out;

        uniform mat4 _mvp;

        void main(void)
        {
            vec4 side1 = geom_in[1].vertex - geom_in[0].vertex;
            vec4 side2 = geom_in[1].vertex - geom_in[2].vertex;
            vec3 faceNormal = cross(side1.xyz, side2.xyz);

            for (int i = 0; i < 3; ++i)
            {
                vec4 vertex = _mvp * geom_in[i].vertex;
                geom_out.vertex = vertex;
                geom_out.id = geom_in[0].id;
                geom_out.visibility = geom_in[i].visibility;
                vec4 projectedNormal = _mvp * vec4(faceNormal, 0.0); // No normal matrix, should do as long as there is no scaling
                geom_out.normal = normalize(projectedNormal.xyz / projectedNormal.w);

                gl_Position = vertex;
                EmitVertex();
            }

            EndPrimitive();
        }
    )"};

    static string SHADER_FRAGMENT_VISIBILITY {R"(
        #version 430 core

        in GEOM_OUT
        {
            flat vec4 vertex;
            flat vec3 normal;
            flat int id;
            flat float visibility;
        } frag_in;

        layout (location = 0) out vec4 fragColor;
        layout (location = 1) out int fragId;
        layout (location = 2) out float fragAngleToNormal;

        void main(void)
        {
            fragColor = vec4(1.0, 0.0, 0.0, 1.0);
            fragColor.rgb *= dot(vec3(0.0, 0.0, 1.0), frag_in.normal);

            if (frag_in.visibility == 0.0)
                fragId = frag_in.id;

            fragAngleToNormal = dot(vec3(0.0, 0.0, 1.0), frag_in.normal) * 255.0 ;
        }
    )"};

    // Texcoord calculation from visibility
    static string SHADER_COMPUTE_TEXCOORDS {R"(
        #version 430 core

        #extension GL_ARB_compute_shader : enable
        #extension GL_ARB_shader_storage_buffer_object : enable

        layout(local_size_x = 32, local_size_y = 32) in;

        layout(binding = 1) uniform isampler2D imgId;
        layout(binding = 2) uniform sampler2D imgAngleToNormal;

        layout(std430, binding = 0) buffer vertexBuffer
        {
            vec4 vertex[];
        };

        layout(std430, binding = 2) buffer visibilityBuffer
        {
            float vertexVisible[];
        };

        layout(std430, binding = 3) buffer texBuffer
        {
            vec2 vertexTexCoords[];
        };

        layout(std430, binding = 4) buffer angleBuffer
        {
            float bestAngleToNormal[];
        };

        uniform float _cameraId;
        uniform float _cameraNumber;
        uniform vec2 _texSize;
        uniform mat4 _mvp;

        void main(void)
        {
            vec2 pixCoords = gl_WorkGroupID.xy * vec2(32.0) + gl_LocalInvocationID.xy;
            vec2 texCoords = pixCoords / _texSize;
            int primitiveID = texelFetch(imgId, ivec2(pixCoords), 0).r; // * 255.0;
            float angleToNormal = texelFetch(imgAngleToNormal, ivec2(pixCoords), 0).r / 255.0;

            if (all(lessThan(pixCoords.xy, _texSize.xy))) // && primitiveID > 0.0)
            {
                float ratio = _texSize.x / _texSize.y;

                for (int idx = primitiveID * 3; idx < primitiveID * 3 + 3; ++idx)
                {
                    // If the face is visible from any other camera previously computed
                    if (vertexVisible[idx] > 0.5f)
                    {
                        // Check whether current camera has a better orientation than the previous camera
                        if (angleToNormal <= 0.f)
                            return;

                        if (angleToNormal <= bestAngleToNormal[idx])
                            return;
                    }

                    bestAngleToNormal[idx] = angleToNormal;

                    vertexVisible[idx] = _cameraId + 1.f;
                    vec4 projectedPoint = _mvp * vertex[idx];
                    projectedPoint = projectedPoint / projectedPoint.w;

                    projectedPoint.y *= ratio;
                    projectedPoint.xy = (projectedPoint.xy + vec2(1.0)) / 2.0;
                    projectedPoint.x /= _cameraNumber;
                    projectedPoint.x += _cameraId / _cameraNumber;

                    vertexTexCoords[idx] = projectedPoint.xy;
                }
            }
        }
    )"};

    /*********/
    ColorizeGLImpl::ColorizeGLImpl()
    {
        console::setVerbosityLevel(console::L_ERROR);

        _glEngine = make_shared<GLEngine>();
        _glEngine->setBufferNumber(5);
        _glEngine->setBufferSize(1024, 1024);
        _glEngine->setColorBuffers({GL_RGBA, GL_R32I, GL_R32F});

        _shaderVisibility = make_shared<Shader>(_glEngine, Shader::prgGraphic);
        _shaderVisibility->setSource(SHADER_VERTEX_VISIBILITY, Shader::vertex);
        _shaderVisibility->setSource(SHADER_GEOMETRY_VISIBILITY, Shader::geometry);
        _shaderVisibility->setSource(SHADER_FRAGMENT_VISIBILITY, Shader::fragment);
        _shaderVisibility->compileProgram();
        _shaderVisibility->setSideness(Shader::inverted);

        _shaderComputeTexCoords = make_shared<Shader>(_glEngine, Shader::prgCompute);
        _shaderComputeTexCoords->setSource(SHADER_COMPUTE_TEXCOORDS, Shader::compute);
        _shaderComputeTexCoords->compileProgram();

        if (_glEngine)
            _ready = true;
    }

    /*********/
    ColorizeGLImpl::~ColorizeGLImpl()
    {
    }

    /*********/
    vector<unsigned char> ColorizeGLImpl::getTexture(unsigned int& width, unsigned int& height)
    {
        width = _mergedWidth;
        height = _mergedHeight;
        return _mergedImage;
    }

    /*********/
    unsigned long long ColorizeGLImpl::getTexturedMesh(std::vector<unsigned char>& serialization, bool threaded)
    {
        if (!_threaded)
        {
            pcl::TextureMesh::Ptr colorizedMesh = boost::make_shared<pcl::TextureMesh>();
            getTexturedMesh(colorizedMesh);

            serialization = _meshSerializer.serialize(colorizedMesh, _timestamp);
        }
        else
        {
            // starting from the blanks stored in _multiBlankMesh, produce a serialized MCTM-texMesh
            // and merge the textures into _mergedImage
            int nsubmeshes = _multiBlankMesh.size();
            vector<TextureMesh::Ptr> colorizedMeshes(nsubmeshes); // is this valid? initialization

            TRACE(auto timenow = chrono::high_resolution_clock::now();
                  int time_ms = chrono::duration_cast<chrono::milliseconds>(timenow.time_since_epoch()).count();)

            // first, colorize each submesh (could be performed concurrently)
            for (int i=0; i<nsubmeshes; i++)
            {
                colorizedMeshes[i] = boost::make_shared<pcl::TextureMesh>();
                computeTextureCoordinates(_multiBlankMesh[i], colorizedMeshes[i]);
            }

            TRACE(timenow = chrono::high_resolution_clock::now();
                  int time_ms2 = chrono::duration_cast<chrono::milliseconds>(timenow.time_since_epoch()).count();)
            TRACE(        cout << time_ms2-time_ms << "ms for Colorize colorization"  << endl;)

            // serialize our colorized multi texMesh 
            serialization = _meshSerializer.serialize(colorizedMeshes, 0); // timestamp=0 (unused in current implementation)

            TRACE(timenow = chrono::high_resolution_clock::now();
                  int time_ms3 = chrono::duration_cast<chrono::milliseconds>(timenow.time_since_epoch()).count();)
            TRACE(        cout << time_ms3-time_ms2 << "ms for Colorize serialization"  << endl;)

            // finally, merge the textures into _mergedImage
            mergeImages();
        }

        return _timestamp;
    }

    /*********/
    unsigned long long ColorizeGLImpl::computeTextureCoordinates(pcl::TextureMesh::Ptr& inputMesh, pcl::TextureMesh::Ptr& outputMesh)
    {
        // turns a blank inputMesh into a textured outputMesh
        
        // calculates the texture coordinates for each vertex of the input mesh 
        // based on stored camera information
        
        if (!inputMesh)
            return 0;

        lock_guard<mutex> lock(_mutex);

        // Upload the mesh
        pcl::PointCloud<pcl::PointXYZ> cloud;
        pcl::fromPCLPointCloud2(inputMesh->cloud, cloud);

        // The three buffers to be uploaded
        std::vector<float> meshBuffer;
        std::vector<int> facesId;
        std::vector<float> vertexVisibility;
        std::vector<float> texCoords;
        std::vector<float> angleToNormal;
        int currentId = 0;

        int polygonNbr = 0;
        for (auto& mesh : inputMesh->tex_polygons)
        {
            polygonNbr += mesh.size();
            for (auto& polygon : mesh)
            {
                for (auto& vertexId : polygon.vertices)
                {
                    meshBuffer.push_back(cloud.points[vertexId].x);
                    meshBuffer.push_back(cloud.points[vertexId].y);
                    meshBuffer.push_back(cloud.points[vertexId].z);
                    meshBuffer.push_back(1.f);

                    facesId.push_back(currentId);
                }
                currentId++;
            }
        }
        vertexVisibility.resize(meshBuffer.size() / 4, 0.f);
        texCoords.resize(meshBuffer.size() / 2, 0.f);
        angleToNormal.resize(meshBuffer.size() / 4, -1.f);

        if (polygonNbr == 0)
            return 0;

        _glEngine->setBuffer(meshBuffer, 0, 4);
        _glEngine->setBuffer(facesId, 1, 1);
        _glEngine->setBuffer(vertexVisibility, 2, 1);
        _glEngine->setBuffer(texCoords, 3, 2);
        _glEngine->setBuffer(angleToNormal, 4, 1);

        // Compute the visibility and the texture coordinates
        for (int camIdx = 0; camIdx < _images.size(); ++camIdx)
        {
            float focalLength = atan(((float)_dims[camIdx][0] * 0.5) / (float)_calibrationParams[camIdx].rgb_focal) * 2.0f;

            //glm::mat4 projMatrix = glm::perspective(focalLength, (float)_dims[camIdx][0] / (float)_dims[camIdx][1], 0.01f, 1000.f);;
            glm::mat4 projMatrix = glm::perspective(focalLength, 1.0f, 0.01f, 1000.f);;
            glm::mat4 mvMatrix = getTransformationForCamera(camIdx);
            glm::mat4 mvpMatrix = projMatrix * mvMatrix;

            // Render the faces id
            _shaderVisibility->setUniform("_mvp", Values(glm::value_ptr(mvpMatrix), glm::value_ptr(mvpMatrix) + 16));
            _glEngine->setShader(_shaderVisibility);
            _glEngine->run();

            // Update the vertex visibility
            // and compute the texture coordinates (if not visible from previous cameras)
            _shaderComputeTexCoords->setUniform("_cameraId", {(float)camIdx});
            _shaderComputeTexCoords->setUniform("_cameraNumber", {(float)_images.size()});
            _shaderComputeTexCoords->setUniform("_texSize", {(float)_dims[camIdx][0], (float)_dims[camIdx][1]});
            _shaderComputeTexCoords->setUniform("_mvp", Values(glm::value_ptr(mvpMatrix), glm::value_ptr(mvpMatrix) + 16));
            _shaderComputeTexCoords->setComputeSize(32, 32);
            _glEngine->setShader(_shaderComputeTexCoords);
            _glEngine->run();
        }

        // Download the computed tex coords
        texCoords = _glEngine->getFeedbackBuffer<float>(3);

        // Write out the output mesh
        outputMesh->tex_polygons.clear();
        outputMesh->tex_coordinates.clear();
        outputMesh->tex_materials.clear();

        outputMesh->tex_polygons.emplace_back();
        outputMesh->tex_coordinates.emplace_back();
        cloud = pcl::PointCloud<pcl::PointXYZ>();
        cloud.reserve(meshBuffer.size() / 4);

        for (unsigned int p = 0; p < meshBuffer.size() / 4; p += 3)
        {
            pcl::Vertices vertices;

            for (unsigned int v = 0; v < 3; ++v)
            {
                int vertexPtr = (p + v) * 4; // 4 coordinates per vertex, the fourth one is not kept
                cloud.points.push_back(pcl::PointXYZ(meshBuffer[vertexPtr], meshBuffer[vertexPtr + 1], meshBuffer[vertexPtr + 2]));

                vertices.vertices.push_back(cloud.points.size() - 1);

                Eigen::Vector2f texCoord;
                texCoord[0] = texCoords[(p + v) * 2];
                texCoord[1] = texCoords[(p + v) * 2 + 1];
                outputMesh->tex_coordinates[0].push_back(texCoord);
            }

            outputMesh->tex_polygons[0].push_back(vertices);
        }

        pcl::toPCLPointCloud2(cloud, outputMesh->cloud);
        return 0;
    }


    /*********/
    unsigned long long ColorizeGLImpl::getTexturedMesh(pcl::TextureMesh::Ptr mesh)
    {
        // calculates the texture coordinates for each vertex based on camera information and vertex positions
        // starting from the blank geometry stored in _mesh
        computeTextureCoordinates(_mesh, mesh);
 
        // And we merge all input textures into a single one
        mergeImages();

        return _timestamp;
    }

   /*********/
    void ColorizeGLImpl::mergeImages()
    {
        // merge the stored individual images into _mergedImage
        // (we still only support input RGB images)
        unsigned int nbrCameras = _dims.size();
        unsigned int maxWidth = 0;
        unsigned int maxHeight = 0;
        for (auto& dim : _dims)
        {
            maxWidth = std::max(maxWidth, dim[0]);
            maxHeight = std::max(maxHeight, dim[1]);
        }

        unsigned int pixelSize = 3;
        unsigned int mergedSize = maxWidth * maxHeight * pixelSize * nbrCameras;
        if (_mergedImage.size() != mergedSize)
            _mergedImage.resize(mergedSize, 0);

        _mergedWidth = maxWidth * nbrCameras;
        _mergedHeight = maxHeight;

        unsigned int step = maxWidth * nbrCameras * pixelSize;
        for (unsigned int i = 0; i < _images.size(); ++i)
        {
            unsigned int shift = maxWidth * i * pixelSize;
            for (unsigned int y = 0; y < _dims[i][1]; ++y)
                memcpy(_mergedImage.data() + y * step + shift,
                       _images[i].data() + y * _dims[i][0] * pixelSize,
                       _dims[i][0] * pixelSize * sizeof(unsigned char));
        }
        
    }

    /*************/
    glm::mat4 ColorizeGLImpl::getTransformationForCamera(unsigned int index)
    {
        glm::mat4 translation = glm::translate(glm::mat4(1.f), glm::vec3(-_calibrationParams[index].offset_x + _calibrationParams[index].rgbd_distance,
                                                                         -_calibrationParams[index].offset_y,
                                                                         -_calibrationParams[index].offset_z));

        float angle = M_PI - _calibrationParams[index].rot_x * (M_PI / 180.f);
        glm::mat4 rotationX = glm::rotate(glm::mat4(1.f), angle, glm::vec3(1.f, 0.f, 0.f));

        angle = -_calibrationParams[index].rot_y * (M_PI / 180.f);
        glm::mat4 rotationY = glm::rotate(glm::mat4(1.f), angle, glm::vec3(0.f, 1.f, 0.f));

        angle = -_calibrationParams[index].rot_z * (M_PI / 180.f);
        glm::mat4 rotationZ = glm::rotate(glm::mat4(1.f), angle, glm::vec3(0.f, 0.f, 1.f));

        auto transformation = rotationX * rotationY * rotationZ * translation;

        return transformation;
    }

    /*********/
    void ColorizeGLImpl::setInput(const vector<unsigned char>& mesh, const vector<vector<unsigned char>>& images, const vector<vector<unsigned int>>& dims)
    {
        if (images.size() != dims.size())
            return;
        for (int i = 0; i < dims.size(); ++i)
            if (dims[i].size() != 3)
                return;

        lock_guard<mutex> lock(_mutex);

        TextureMesh::Ptr inputMesh = _meshSerializer.deserializeAsTextured(mesh, _timestamp);
        _mesh = inputMesh;

        _images = images;
        _dims = dims;
    }

    /*********/
    void ColorizeGLImpl::setInput(TextureMesh::Ptr mesh, const std::vector<std::vector<unsigned char>>& images, const std::vector<std::vector<unsigned int>>& dims)
    {
        if (images.size() != dims.size())
            return;
        for (int i = 0; i < dims.size(); ++i)
            if (dims[i].size() != 3)
                return;

        lock_guard<mutex> lock(_mutex);

        _mesh = boost::make_shared<TextureMesh>();
        _mesh->cloud = mesh->cloud;
        _mesh->tex_polygons = mesh->tex_polygons;
        _mesh->tex_coordinates = mesh->tex_coordinates;

        _images = images;
        _dims = dims;
    }

    /*********/
    void ColorizeGLImpl::setInput(PolygonMesh::Ptr mesh, const std::vector<std::vector<unsigned char>>& images, const std::vector<std::vector<unsigned int>>& dims)
    {
        if (images.size() != dims.size())
            return;
        for (int i = 0; i < dims.size(); ++i)
            if (dims[i].size() != 3)
                return;

        lock_guard<mutex> lock(_mutex);

        _mesh = boost::make_shared<TextureMesh>();
        _mesh->cloud = mesh->cloud;
        _mesh->tex_polygons.push_back(mesh->polygons);

        _images = images;
        _dims = dims;
        _threaded = false;
    }

    /*********/
    // stores blank texturemesh versions of the multimesh given as input, and copy input images into the store too
    void ColorizeGLImpl::setInput(vector<PolygonMesh::Ptr> multimesh, const vector<vector<unsigned char> >& images, const vector<vector<unsigned int> >& dims)
    {
        if (images.size() != dims.size()) 
        {
            TRACE(cerr << "ColorizeGLImpl: size mismatch " << dims.size() << endl;)
            return;
        }
        for (int i = 0; i < dims.size(); ++i)
            if (dims[i].size() != 3)
            {
                TRACE(cerr << "CGLI: size is not 3, it is " << dims[i].size() << endl;)
                return;
            }

        lock_guard<mutex> lock(_mutex);

        _multiBlankMesh.clear();
        for (auto& submesh: multimesh)
        {
            // make a new blank texture mesh and copy geometry into it
            TextureMesh::Ptr newTexSubmesh = boost::make_shared<TextureMesh>();
            newTexSubmesh->cloud = submesh->cloud;
            newTexSubmesh->tex_polygons.push_back(submesh->polygons);
          
            // append it to our store
            _multiBlankMesh.push_back(newTexSubmesh);
        }
        _images = images;
        _dims = dims;
        _threaded = true;
    }


} // end of namespace
