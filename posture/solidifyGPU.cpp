#include "solidifyGPU.h"
#include "solidifyGPU-impl.h"

using namespace std;

namespace posture
{
    /*************/
    SolidifyGPU::SolidifyGPU()
    {
        _impl = make_shared<SolidifyGPUImpl>();
    }

    /*************/
    SolidifyGPU::~SolidifyGPU()
    {
    }

    /*************/
    unsigned long long SolidifyGPU::getMesh(std::vector<unsigned char>& mesh, bool threaded) const
    {
        if (!_impl)
            return 0;

        return _impl->getMesh(mesh, threaded);
    }

    /*************/
    unsigned long long SolidifyGPU::getMesh(pcl::PolygonMesh::Ptr& mesh) const
    {
        if (!_impl)
            return 0;

        return _impl->getMesh(mesh);
    }

    /*************/
    unsigned long long SolidifyGPU::getMesh(std::vector<pcl::PolygonMesh::Ptr>& multimesh) const
    {
        if (!_impl)
            return 0;

        return _impl->getMesh(multimesh);        
    }

    /*************/
    void SolidifyGPU::setGridResolution(float res) const
    {
        if (!_impl)
            return;

        _impl->setGridResolution(res);
    }

    /*************/
    void SolidifyGPU::setGridSize(float size) const
    {
        if (!_impl)
            return;

        _impl->setGridSize(size);
    }

    /*************/
    void SolidifyGPU::setGridSizeX(float size) const
    {
        if (!_impl)
            return;

        _impl->setGridSizeX(size);
    }

    /*************/
    void SolidifyGPU::setGridSizeY(float size) const
    {
        if (!_impl)
            return;

        _impl->setGridSizeY(size);
    }

    /*************/
    void SolidifyGPU::setGridSizeZ(float size) const
    {
        if (!_impl)
            return;

        _impl->setGridSizeZ(size);
    }

    /*************/
    void SolidifyGPU::setDepthMapNbr (unsigned int dpmNbr) const
    {
        if (!_impl)
            return;

        _impl->setDepthMapNbr (dpmNbr);
    }

    /*************/
    void SolidifyGPU::setInputDepthMap(int index, std::vector<unsigned short>& depth, int width, int height) const
    {
        if (!_impl)
            return;

        return _impl->setInputDepthMap(index, depth, width, height);
    }

    /*************/
    void SolidifyGPU::setInputDepthMap(int index, std::vector<unsigned char>& depth, int width, int height) const
    {
        if (!_impl)
            return;

        return _impl->setInputDepthMap(index, depth, width, height);
    }

    /*************/
    void SolidifyGPU::setCompressMesh(bool active) const
    {
        if (!_impl)
            return;

        _impl->setCompressMesh(active);
    }

    /*************/
    void SolidifyGPU::setSaveMesh(bool active) const
    {
        if (!_impl)
            return;

        _impl->setSaveMesh(active);
    }

    /*************/
    void SolidifyGPU::setCalibration(const vector<CalibrationParams>& calibrations) const
    {
        if (!_impl)
            return;
        
        return _impl->setCalibration(calibrations);
    }

    /*************/
    void SolidifyGPU::setDepthFiltering(int size, float sigmaPos, float sigmaValue) const
    {
        if (!_impl)
            return;

        _impl->setDepthFiltering(size, sigmaPos, sigmaValue);
    }

    /*************/
    void SolidifyGPU::setHoleFillingIterations(int iter)
    {
        if (!_impl)
            return;

        _impl->setHoleFillingIterations(iter);
    }

} // end of namespace
