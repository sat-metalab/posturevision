/*
 * Copyright (C) 2014 Emmanuel Durand (https://www.emmanueldurand.net)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

#include "display-impl.h"

#include <iostream>

#include <boost/make_shared.hpp>

using namespace std;
using namespace pcl;

namespace posture
{
    /*********/
    DisplayImpl::DisplayImpl()
    {
        init();
    }

    /*********/
    DisplayImpl::DisplayImpl(string name)
    {
        _windowName = name;
        init();
    }

    /*********/
    DisplayImpl::~DisplayImpl()
    {
        _viewer.reset();
    }

    /*********/
    void DisplayImpl::init()
    {
        _viewer = make_shared<pcl::visualization::CloudViewer>(_windowName);
        _viewer->runOnVisualizationThread(boost::bind(&DisplayImpl::mesh_callback, this, _1), "mesh_callback");
        _viewer->runOnVisualizationThread(boost::bind(&DisplayImpl::normal_callback, this, _1), "normal_callback");
        _viewer->runOnVisualizationThread(boost::bind(&DisplayImpl::textureMesh_callback, this, _1), "textureMesh_callback");
        _mesh = boost::make_shared<pcl::PolygonMesh>();
    }

    /*********/
    void DisplayImpl::setInputCloud(const vector<char>& cloud, const bool compressed)
    {
        unsigned long long captureStamp;
        PointCloud<PointXYZRGB>::Ptr pclCloud = boost::make_shared<PointCloud<PointXYZRGB>>();

        _mutex.lock();
        _cloud = _pclSerializer.deserialize(cloud, compressed, captureStamp);
        _mutex.unlock();

        copyPointCloud(*_cloud, *pclCloud);
        _viewer->showCloud(pclCloud);
    }

    /*************/
    void DisplayImpl::setInputCloud(const pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud)
    {
        PointCloud<PointXYZRGB>::Ptr pclCloud = boost::make_shared<PointCloud<PointXYZRGB>>();

        _mutex.lock();
        _cloud = cloud;
        _mutex.unlock();

        copyPointCloud(*_cloud, *pclCloud);
        _viewer->showCloud(pclCloud);
    }

    /*********/
    void DisplayImpl::setPolygonMesh(const vector<unsigned char>& mesh)
    {
        lock_guard<mutex> lock(_mutex);
        unsigned long long timestamp;
        _mesh = _meshSerializer.deserialize(mesh, timestamp);
    }

    /*************/
    void DisplayImpl::setPolygonMesh(const PolygonMesh::Ptr mesh)
    {
        lock_guard<mutex> lock(_mutex);
        *_mesh = *mesh;
    }

    /*************/
    void DisplayImpl::setTextureMesh(const TextureMesh::Ptr mesh)
    {
        lock_guard<mutex> lock(_mutex);
        _textureMesh = mesh;
    }

    /*********/
    bool DisplayImpl::wasStopped()
    {
        if (_viewer.get() == nullptr)
            return false;
            
        return _viewer->wasStopped();
    }

    /*********/
    void DisplayImpl::normal_callback(pcl::visualization::PCLVisualizer& viz)
    {
        if (!_cloud)
            return;

        lock_guard<mutex> lock(_mutex);
        viz.removePointCloud("normalCloud");
        viz.addPointCloudNormals<PointXYZRGBNormal>(_cloud, 1, 0.02f, "normalCloud");
    }

    /*********/
    void DisplayImpl::mesh_callback(pcl::visualization::PCLVisualizer& viz)
    {
        if (!_mesh || _mesh->polygons.size() == 0)
            return;

        lock_guard<mutex> lock(_mutex);
        viz.removePolygonMesh();
        viz.addPolygonMesh(*_mesh);
        if (_isWireframe)
            viz.setRepresentationToWireframeForAllActors();
    }

    /*********/
    void DisplayImpl::textureMesh_callback(pcl::visualization::PCLVisualizer& viz)
    {
        if (!_textureMesh || _textureMesh->tex_polygons.size() == 0 || _textureMesh->cloud.width * _textureMesh->cloud.height == 0)
            return;

        lock_guard<mutex> lock(_mutex);
        viz.addTextureMesh(*_textureMesh, "textureMesh");
    }

    /*********/
    void DisplayImpl::setWireframe(bool enable)
    {
        _isWireframe = enable;
    }

} // end of namespace
