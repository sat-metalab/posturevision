#include "glEngine.h"

#include <iostream>
#include <stdlib.h>

#define VIEW_RENDER_WINDOW false

using namespace std;

namespace posture
{

/*************/
GLEngine::GLEngine()
{
    string display = "";
    if (getenv("DISPLAY"))
        display = getenv("DISPLAY");

    if (display == "")
    {
        cout << "DISPLAY environment variable not set. Setting it to :0" << endl;
        setenv("DISPLAY", ":0", 1);
    }

    glfwSetErrorCallback(GLEngine::glfwErrorCallback);

    if (!glfwInit())
    {
        cout << "Error while initializing GLFW" << endl;
        return;
    }

    _glVersion = findGLVersion();
    
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, _glVersion[0]);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, _glVersion[1]);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_DEPTH_BITS, 32);

    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, false);
    glfwWindowHint(GLFW_VISIBLE, VIEW_RENDER_WINDOW);
    
    _glWindow = glfwCreateWindow(_glWidth, _glHeight, "glEngine", nullptr, nullptr);
    if (!_glWindow)
    {
        cout << "Unable to create a GLFW window" << endl;
        return;
    }
    
    if (!initializeFBO())
    {
        cout << "Unable to initialize GL render pipeline" << endl;
        return;
    }

    glfwMakeContextCurrent(_glWindow);
    glfwSwapInterval(1);
    glfwMakeContextCurrent(nullptr);

#ifdef DEBUGGL
    glDebugMessageCallback(GLEngine::glMsgCallback, (void*)this);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_MEDIUM, 0, nullptr, GL_TRUE);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_HIGH, 0, nullptr, GL_TRUE);
#endif
}

/*************/
GLEngine::~GLEngine()
{
    glfwMakeContextCurrent(_glWindow);
    cleanFbo();
    cleanBuffers();
    glfwMakeContextCurrent(nullptr);
    glfwDestroyWindow(_glWindow);
}

/*************/
void GLEngine::cleanFbo()
{
    glDeleteFramebuffers(1, &_fbo);
    glDeleteTextures(1, &_glDepthTex);
    if (_glColorTex.size())
        glDeleteTextures(_glColorTex.size(), _glColorTex.data());
}

/*************/
void GLEngine::cleanBuffers()
{
    if (_bufferObjects.size())
        glDeleteBuffers(_bufferObjects.size(), _bufferObjects.data());
}

/*************/
void GLEngine::setBufferSize(int width, int height)
{
    _glWidth = std::max(256, width);
    _glHeight = std::max(256, height);
    glfwSetWindowSize(_glWindow, _glWidth, _glHeight);

    setColorBuffers(_glColorFormats);
}

/*************/
void GLEngine::bindColorBuffers()
{
    GLint glId = 0;

    for (auto& tex : _glColorTex)
    {
        glActiveTexture(GL_TEXTURE0 + glId);
        glBindTexture(GL_TEXTURE_2D, tex);
        glId++;
    }
}

/*************/
void GLEngine::unbindColorBuffers()
{
    for (auto& tex : _glColorTex)
        glBindTexture(GL_TEXTURE_2D, 0);
}

/*************/
vector<int> GLEngine::findGLVersion()
{
    vector<vector<int>> glVersionList {{4, 5}, {4, 4}, {4, 3}, {3, 3}, {3, 2}};
    vector<int> detectedVersion {0, 0};

    for (auto version : glVersionList)
    {
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, version[0]);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, version[1]);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_SRGB_CAPABLE, GL_TRUE);
        glfwWindowHint(GLFW_DEPTH_BITS, 32);
        glfwWindowHint(GLFW_VISIBLE, false);
        GLFWwindow* window = glfwCreateWindow(512, 512, "test_window", NULL, NULL);

        if (window)
        {
            detectedVersion = version;
            glfwDestroyWindow(window);
            break;
        }
    }

    return detectedVersion;
}

/*************/
void GLEngine::glfwErrorCallback(int code, const char* msg)
{
    clog << LogPriority::WARNING << __FUNCTION__ << " - " << msg << endl;
}

/*************/
void GLEngine::glMsgCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
{
    string typeString {""};
    LogPriority logType;
    switch (type)
    {
    case GL_DEBUG_TYPE_ERROR:
        typeString = "Error";
        logType = LogPriority::ERROR;
        break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
        typeString = "Deprecated behavior";
        logType = LogPriority::WARNING;
        break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
        typeString = "Undefined behavior";
        logType = LogPriority::ERROR;
        break;
    case GL_DEBUG_TYPE_PORTABILITY:
        typeString = "Portability";
        logType = LogPriority::WARNING;
        break;
    case GL_DEBUG_TYPE_PERFORMANCE:
        typeString = "Performance";
        logType = LogPriority::WARNING;
        break;
    case GL_DEBUG_TYPE_OTHER:
        typeString = "Other";
        logType = LogPriority::MESSAGE;
        break;
    }

    clog << logType << "GL::debug - [" << typeString << "] - " << message << endl;
}

/*************/
void GLEngine::lockContext()
{
    if (!_isContextLocked)
    {
        glfwMakeContextCurrent(_glWindow);
        _isContextLocked = true;
        _isManuallyLocked = true;
    }
}

/*************/
void GLEngine::unlockContext()
{
    if (_isManuallyLocked)
    {
        glfwMakeContextCurrent(nullptr);
        _isContextLocked = false;
        _isManuallyLocked = false;
    }
}

/*************/
void GLEngine::run()
{
    if (!_shader)
        return;

    if (_shader->getProgramType() != Shader::prgCompute)
    {
        runGraphics();
    }
    else
    {
        runCompute();
    }
}

/*************/
void GLEngine::runCompute()
{
    if (!_isManuallyLocked)
    {
        _isContextLocked = true;
        glfwMakeContextCurrent(_glWindow);
    }

    bindColorBuffers();

    // Activate the vertex buffers
    for (uint32_t i = 0; i < _bufferObjects.size(); ++i)
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, i, _bufferObjects[i]);

    _shader->doCompute();

    // Deactivate the vertex buffers
    for (uint32_t i = 0; i < _bufferObjects.size(); ++i)
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, i, 0);

    unbindColorBuffers();

    if (!_isManuallyLocked)
    {
        _isContextLocked = false;
        glfwMakeContextCurrent(nullptr);
    }
}

/*************/
void GLEngine::runGraphics()
{
    if (!_isManuallyLocked)
    {
        _isContextLocked = true;
        glfwMakeContextCurrent(_glWindow);
    }
    
    glViewport(0, 0, _glWidth, _glHeight);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _fbo);
    vector<GLenum> fboBuffers;
    for (unsigned int i = 0; i < _glColorTex.size(); ++i)
        fboBuffers.push_back(GL_COLOR_ATTACHMENT0 + i);

    glDrawBuffers(_glColorTex.size(), fboBuffers.data());
    glEnable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Activate the vertex array
    if (_vertexArray == 0)
        glGenVertexArrays(1, &_vertexArray);
    glBindVertexArray(_vertexArray);

    // Activate the vertex buffers
    for (uint32_t i = 0; i < _bufferObjects.size(); ++i)
    {
        if (_bufferObjectParams[i].type == GL_TRANSFORM_FEEDBACK_BUFFER)
            glBindBufferBase(_bufferObjectParams[i].type, 0, _bufferObjects[i]);
        else
        {
            glBindBuffer(_bufferObjectParams[i].type, _bufferObjects[i]);
            glVertexAttribPointer((GLuint)i, _bufferObjectParams[i].elemSize, _bufferObjectParams[i].elemType, GL_FALSE, 0, 0);
            glEnableVertexAttribArray((GLuint)i);
        }
    }

    _shader->activate();
    _shader->updateUniforms();

    int verticesNbr = numeric_limits<int>::max();
    for (auto& params : _bufferObjectParams)
        if (params.type == GL_ARRAY_BUFFER)
            verticesNbr = std::min(verticesNbr, params.elemNbr);

    if (_shader->getProgramType() == Shader::prgFeedback)
    {
        glDrawArrays(GL_PATCHES, 0, verticesNbr);
    }
    else
    {
        glDrawArrays(GL_TRIANGLES, 0, verticesNbr);
    }

    _shader->deactivate();

    // Deactivate the vertex buffers
    for (uint32_t i = 0; i < _bufferObjects.size(); ++i)
        glDisableVertexAttribArray((GLuint)i);
    glBindVertexArray(0);

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

    if (VIEW_RENDER_WINDOW)
        drawToWindow();

    glFinish();

    if (!_isManuallyLocked)
    {
        _isContextLocked = false;
        glfwMakeContextCurrent(nullptr);
    }
}

/*************/
void GLEngine::drawToWindow()
{
    glBindFramebuffer(GL_READ_FRAMEBUFFER, _fbo);
    glReadBuffer(GL_COLOR_ATTACHMENT0);
    glDrawBuffer(GL_BACK);
    glBlitFramebuffer(0, 0, _glWidth, _glHeight,
                      0, 0, _glWidth, _glHeight,
                      GL_COLOR_BUFFER_BIT, GL_NEAREST);

    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
    glfwSwapBuffers(_glWindow);
}

/*************/
bool GLEngine::setShader(const shared_ptr<Shader>& shader)
{
    _shader = shader;
    return true;
}

/*************/
void GLEngine::setColorBuffers(vector<GLint> formats)
{
    if (!_isManuallyLocked)
    {
        _isContextLocked = true;
        glfwMakeContextCurrent(_glWindow);
    }

    _glColorFormats = formats;
    int number = formats.size();

    glDeleteTextures(_glColorTex.size(), &_glColorTex[0]);
    _glColorTex.clear();

    if (number > 0)
    {
        _glColorTex.resize(number);
        glGenTextures(number, &_glColorTex[0]);
        for (unsigned int i = 0; i < number; ++i)
            initializeTexture(_glColorTex[i], _glColorFormats[i]);
    }

    cleanFbo();
    initializeFBO();

    if (!_isManuallyLocked)
    {
        _isContextLocked = false;
        glfwMakeContextCurrent(nullptr);
    }
}

/*************/
void GLEngine::resetBuffers()
{
    if (_bufferObjects.size() > 0)
    {
        glDeleteBuffers(_bufferObjects.size(), &_bufferObjects[0]);
        _bufferObjects.clear();
        _bufferObjectParams.clear();
    }
}

/*************/
void GLEngine::setBufferNumber(unsigned int number)
{
    if (!_isManuallyLocked)
    {
        _isContextLocked = true;
        glfwMakeContextCurrent(_glWindow);
    }

    if (number == 0)
    {
        glDeleteBuffers(_bufferObjects.size(), &_bufferObjects[0]);
        return;
    }
    else if (number > _bufferObjects.size())
    {
        int previousSize = _bufferObjects.size();
        _bufferObjects.resize(number);
        _bufferObjectParams.resize(number);
        glGenBuffers(number - previousSize, &_bufferObjects[previousSize]);
    }
    else if (number < _bufferObjects.size())
    {
        glDeleteBuffers(_bufferObjects.size() - number, &_bufferObjects[number - 1]);
        _bufferObjects.resize(number);
        _bufferObjectParams.resize(number);
    }

    if (!_isManuallyLocked)
    {
        _isContextLocked = false;
        glfwMakeContextCurrent(nullptr);
    }
}

/*************/
void GLEngine::initializeTexture(GLuint tex, GLint format)
{
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    switch (format)
    {
    case GL_RGBA:
        glTexImage2D(GL_TEXTURE_2D, 0, format, _glWidth, _glHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
        break;
    case GL_RGB:
        glTexImage2D(GL_TEXTURE_2D, 0, format, _glWidth, _glHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
        break;
    case GL_R32UI:
        glTexImage2D(GL_TEXTURE_2D, 0, format, _glWidth, _glHeight, 0, GL_RED_INTEGER, GL_INT, nullptr);
        break;
    case GL_R32I:
        glTexImage2D(GL_TEXTURE_2D, 0, format, _glWidth, _glHeight, 0, GL_RED_INTEGER, GL_UNSIGNED_INT, nullptr);
        break;
    case GL_R32F:
        glTexImage2D(GL_TEXTURE_2D, 0, format, _glWidth, _glHeight, 0, GL_RED, GL_FLOAT, nullptr);
        break;
    case GL_RGBA32F:
        glTexImage2D(GL_TEXTURE_2D, 0, format, _glWidth, _glHeight, 0, GL_RGBA, GL_FLOAT, nullptr);
        break;
    default:
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _glWidth, _glHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
    }

    glBindTexture(GL_TEXTURE_2D, 0);
}

/*************/
bool GLEngine::initializeFBO()
{
    glfwMakeContextCurrent(_glWindow);
    
    glGetError();
    glGenFramebuffers(1, &_fbo);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _fbo);
    
    // Create the textures
    glActiveTexture(GL_TEXTURE0);
    
    glGenTextures(1, &_glDepthTex);
    glBindTexture(GL_TEXTURE_2D, _glDepthTex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, _glWidth, _glHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    glGenTextures(_glColorTex.size(), _glColorTex.data());
    for (unsigned int i = 0; i < _glColorTex.size(); ++i)
        initializeTexture(_glColorTex[i], _glColorFormats[i]);
    
    // Connect textures to the FBO
    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, _glDepthTex, 0);
    for (unsigned int i = 0; i < _glColorTex.size(); ++i)
        glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, _glColorTex[i], 0);
    
    // Set clear parameters
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    GLenum error = glGetError();
    if (error)
    {
        cout << "Error while initializing renderer: " << error << endl;
        return false;
    }
    
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE)
    {
        cout << GL_FRAMEBUFFER_COMPLETE << endl;
        cout << "Error while initializing framebuffer object: " << status << endl;
        return false;
    }
    
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    
    glfwMakeContextCurrent(nullptr);
    
    return true;
}

/*************/
bool GLEngine::setBuffer(const void* data, size_t size, size_t typeSize, unsigned int index, int elemSize, GLenum elemType, GLenum type)
{
    if (!_isManuallyLocked)
    {
        _isContextLocked = true;
        glfwMakeContextCurrent(_glWindow);
    }

    if (index > _bufferObjects.size())
        return false;

    if (size == 0)
        return false;

    GLsizeiptr bufferSize;
    if (type == GL_TRANSFORM_FEEDBACK_BUFFER)
        bufferSize = size * typeSize * static_cast<int>(pow(_tessLevel+1, 5.0/2.0));
    else
        bufferSize = size * typeSize;

    glBindBuffer(GL_ARRAY_BUFFER, _bufferObjects[index]);

    if (type == GL_TRANSFORM_FEEDBACK_BUFFER)
        glBufferData(GL_ARRAY_BUFFER, bufferSize, nullptr, GL_STATIC_DRAW);
    else
        glBufferData(GL_ARRAY_BUFFER, bufferSize, data, GL_STATIC_DRAW);


    glBindBuffer(GL_ARRAY_BUFFER, 0);

    _bufferObjectParams[index].elemNbr = size / elemSize;
    _bufferObjectParams[index].elemSize = elemSize;
    _bufferObjectParams[index].elemType = elemType;
    _bufferObjectParams[index].type = type;

    if (!_isManuallyLocked)
    {
        _isContextLocked = false;
        glfwMakeContextCurrent(nullptr);
    }

    return true;
}

/*************/
template<>
bool GLEngine::setBuffer(const vector<char>& data, unsigned int index, int elemSize, GLenum type)
{
    return setBuffer(data.data(), data.size(), sizeof(char), index, elemSize, GL_BYTE, type);
}

/*************/
template<>
bool GLEngine::setBuffer(const vector<unsigned char>& data, unsigned int index, int elemSize, GLenum type)
{
    return setBuffer(data.data(), data.size(), sizeof(unsigned char), index, elemSize, GL_UNSIGNED_BYTE, type);
}

/*************/
template<>
bool GLEngine::setBuffer(const vector<short>& data, unsigned int index, int elemSize, GLenum type)
{
    return setBuffer(data.data(), data.size(), sizeof(short), index, elemSize, GL_SHORT, type);
}

/*************/
template<>
bool GLEngine::setBuffer(const vector<unsigned short>& data, unsigned int index, int elemSize, GLenum type)
{
    return setBuffer(data.data(), data.size(), sizeof(unsigned short), index, elemSize, GL_UNSIGNED_SHORT, type);
}

/*************/
template<>
bool GLEngine::setBuffer(const vector<int>& data, unsigned int index, int elemSize, GLenum type)
{
    return setBuffer(data.data(), data.size(), sizeof(int), index, elemSize, GL_INT, type);
}

/*************/
template<>
bool GLEngine::setBuffer(const vector<unsigned int>& data, unsigned int index, int elemSize, GLenum type)
{
    return setBuffer(data.data(), data.size(), sizeof(unsigned int), index, elemSize, GL_UNSIGNED_INT, type);
}

/*************/
template<>
bool GLEngine::setBuffer(const vector<float>& data, unsigned int index, int elemSize, GLenum type)
{
    return setBuffer(data.data(), data.size(), sizeof(float), index, elemSize, GL_FLOAT, type);
}

} // end of namespace
