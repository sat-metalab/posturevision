#include <iostream>

#include "filter.h"

using namespace std;
using namespace pcl;
using namespace Eigen;

namespace posture
{
    /*********/
    Filter::Filter()
    {
        // Sphere distance function
        _distanceFunctions[std::string("sphere")] = [](const vector<float>& p, const vector<float>& s) -> float {
            if (s.size() < 4)
                return -1.f;
    
            float distance = sqrtf(pow(p[0] - s[1], 2.f) + pow(p[1] - s[2], 2.f) + pow(p[2] - s[3], 2.f));
            return distance - s[0];
        };
        // Infinite cylinder distance function
        _distanceFunctions[std::string("cylinder")] = [](const vector<float>& p, const vector<float>& s) -> float {
            if (s.size() < 5)
                return 0.f;
    
            // s[4] contains the axis index of the cylinder
            if (s[4] == 0.f)
                return sqrtf(pow(p[1] - s[2], 2.f) + pow(p[2] - s[3], 2.f)) - s[0];
            else if (s[4] == 1.f)
                return sqrtf(pow(p[0] - s[1], 2.f) + pow(p[2] - s[3], 2.f)) - s[0];
            else if (s[4] == 2.f)
                return sqrtf(pow(p[0] - s[1], 2.f) + pow(p[1] - s[2], 2.f)) - s[0];
            else
                return 0.f;
        };
    }


    /*********/
    void Filter::applyShapeClipping(PolygonMesh::Ptr& input, const CalibrationParams& calibration)
    {
        // puts NaNs instead of every vertex not in bounding box. 

        PointCloud<PointNormal>::Ptr clipped = boost::make_shared<PointCloud<PointNormal>>();
        fromPCLPointCloud2(input->cloud, *clipped);

        for (auto it = clipped->begin(); it != clipped->end(); ++it)
        {
            auto& vertex = *it;
            if (!isInShapes(vertex, calibration))
            { // one coord might be enough
                vertex.x = numeric_limits<float>::quiet_NaN();
                vertex.y = numeric_limits<float>::quiet_NaN();
                vertex.z = numeric_limits<float>::quiet_NaN();
            }
        }

        //PolygonMesh::Ptr outputMesh = boost::make_shared<PolygonMesh>();
        //*outputMesh = *input;
        toPCLPointCloud2(*clipped, input->cloud);
        filterNaN(input);
    }


    /*********/
    void Filter::applyShapeClipping(TextureMesh::Ptr& input, const CalibrationParams& calibration)
    {
        // puts NaNs instead of every vertex not in bounding box. 

        PointCloud<PointNormal>::Ptr clipped = boost::make_shared<PointCloud<PointNormal>>();
        fromPCLPointCloud2(input->cloud, *clipped);

        for (auto it = clipped->begin(); it != clipped->end(); ++it)
        {
            auto& vertex = *it;
            if (!isInShapes(vertex, calibration))
            { // one coord might be enough
                vertex.x = numeric_limits<float>::quiet_NaN();
                vertex.y = numeric_limits<float>::quiet_NaN();
                vertex.z = numeric_limits<float>::quiet_NaN();
            }
        }

        toPCLPointCloud2(*clipped, input->cloud);
        filterNaN(input);
    }

    /*********/
    void Filter::filterNaN(PolygonMesh::Ptr& mesh)
    {
        // make a new mesh to hold the result
        PolygonMesh::Ptr filteredMesh = boost::make_shared<PolygonMesh>();

        // copy the point cloud from the (PCL2) mesh into a regular PointCloud
        PointCloud<PointXYZ>::Ptr cloud = boost::make_shared<PointCloud<PointXYZ>>();
        fromPCLPointCloud2(mesh->cloud, *cloud);

        TRACE(cerr << " Filter::filterNaN: total vertices in cloud: " << cloud->size() << endl;)

        // this will hold the filtered cloud
        PointCloud<PointXYZ>::Ptr filteredCloud = boost::make_shared<PointCloud<PointXYZ>>();
	
        // the filtered cloud will have fewer vertices; we must map indices
        map<unsigned int, unsigned int> newVertexIndex;

        // we handle all polygons; this will take care of all the vertices too
        for (auto& poly : mesh->polygons)
        {
            // skip this polygon if it has a NaN vertex
            bool isNaN = false;
            for (auto& vertex : poly.vertices)
            {
                if (!isFinite(cloud->at(vertex)))
                    isNaN = true;
            }
            if (isNaN)
	            continue;

            // ok, this polygon is legit
            Vertices face;

            for (auto& vertex : poly.vertices)
            {
                // (note: this is perhaps where we could spot vertex position redundancies, too)
                // if this vertex id is not yet in the index, 
                if (newVertexIndex.find(vertex) == newVertexIndex.end())
                {
                    // append it to the filtered cloud and record its new index
                    filteredCloud->push_back(cloud->at(vertex));
                    newVertexIndex[vertex] = filteredCloud->size() - 1;
                }
                // the face is rebuilt with the new indices
                face.vertices.push_back(newVertexIndex[vertex]);
            }
            filteredMesh->polygons.push_back(face);
        }
        TRACE(cerr << " Filter::filterNaN: total vertices in clipped cloud: " << filteredCloud->size() << endl;)

        // copy the filtered cloud into the mesh we will return 
        toPCLPointCloud2(*filteredCloud, filteredMesh->cloud);
        mesh = filteredMesh;
    
    }

    /*********/
    void Filter::filterNaN(TextureMesh::Ptr& mesh) // NOTE: IGNORES UV COORDINATES
    {
        // make a new mesh to hold the result
        TextureMesh::Ptr filteredMesh = boost::make_shared<TextureMesh>();
        filteredMesh->tex_polygons.resize(1);
        filteredMesh->tex_coordinates.resize(1);

        // copy the point cloud from the (PCL2) mesh into a regular PointCloud
        PointCloud<PointNormal>::Ptr cloud = boost::make_shared<PointCloud<PointNormal>>();
        fromPCLPointCloud2(mesh->cloud, *cloud);

        // this will hold the filtered cloud
        PointCloud<PointNormal>::Ptr filteredCloud = boost::make_shared<PointCloud<PointNormal>>();

        // the filtered cloud will have fewer vertices; we must map indices
        map<unsigned int, unsigned int> vertexIndex;

        // Filter the UVs based on the vertices
        if (mesh->tex_coordinates.size() > 0)
            for (int i = 0; i < cloud->size(); ++i)
                if (isFinite(cloud->at(i)) && i < mesh->tex_coordinates[0].size())
                    filteredMesh->tex_coordinates[0].push_back(mesh->tex_coordinates[0][i]);

        // we handle all polygons; this will take care of all the vertices too
        for (auto& poly : mesh->tex_polygons[0])
        {
            // skip this polygon if it has a NaN vertex
            bool isNaN = false;
            for (auto& vertex : poly.vertices)
            {
                if (!isFinite(cloud->at(vertex)))
                    isNaN = true;
            }
            if (isNaN)
                continue;

            // ok, this polygon is legit
            Vertices face;

            for (auto& vertex : poly.vertices)
            {
                // if this vertex id is not yet in the index, 
                if (vertexIndex.find(vertex) == vertexIndex.end())
                {
                    // append it to the filtered cloud and record its new index
                    filteredCloud->push_back(cloud->at(vertex));
                    vertexIndex[vertex] = filteredCloud->size() - 1;
                }
                face.vertices.push_back(vertexIndex[vertex]);
            }
            // the face is rebuilt with the new indices
            filteredMesh->tex_polygons[0].push_back(face);
        }

        // copy the filtered cloud into the mesh that we will return 
        toPCLPointCloud2(*filteredCloud, filteredMesh->cloud);
        mesh = filteredMesh;
     
    }
} // end of namespace
