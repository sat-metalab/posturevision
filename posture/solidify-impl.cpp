#include "solidify-impl.h"

#include <iostream>
#include <map>

#include <boost/make_shared.hpp>
#include <pcl/io/obj_io.h>

using namespace std;
using namespace pcl;

namespace posture
{
    /*************/
    SolidifyImpl::SolidifyImpl()
    {
        _marchingCubes = boost::make_shared<MarchingCubesHoppe<PointXYZRGBNormal>>();
        _searchTree = boost::make_shared<search::KdTree<PointXYZRGBNormal>>();
        _mesh = boost::make_shared<PolygonMesh>();

        _marchingCubes->setGridResolution(16.0, 16.0, 16.0);
        _marchingCubes->setIsoLevel(0.0);
        _marchingCubes->setSearchMethod(_searchTree);
        _marchingCubes->setPercentageExtendGrid(0.2);
    }
    
    /*************/
    SolidifyImpl::~SolidifyImpl()
    {
        lock_guard<mutex> lock(_mutex);
    }

    /*************/
    void SolidifyImpl::filterNaNFromMesh(PolygonMesh::Ptr mesh)
    {
        PolygonMesh::Ptr filteredMesh = boost::make_shared<PolygonMesh>();

        PointCloud<PointNormal>::Ptr cloud = boost::make_shared<PointCloud<PointNormal>>();
        PointCloud<PointNormal>::Ptr filteredCloud = boost::make_shared<PointCloud<PointNormal>>();
        fromPCLPointCloud2(mesh->cloud, *cloud);

        map<unsigned int, unsigned int> vertexIndex;
        for (auto& poly : mesh->polygons)
        {
            Vertices face;

            bool isNaN = false;
            for (auto& vertex : poly.vertices)
            {
                if (!isFinite(cloud->at(vertex)))
                    isNaN = true;
            }
            if (isNaN)
                continue;

            for (auto& vertex : poly.vertices)
            {
                if (vertexIndex.find(vertex) == vertexIndex.end())
                {
                    filteredCloud->push_back(cloud->at(vertex));
                    vertexIndex[vertex] = filteredCloud->size() - 1;
                }
                face.vertices.push_back(vertexIndex[vertex]);
            }
            filteredMesh->polygons.push_back(face);
        }

        toPCLPointCloud2(*filteredCloud, filteredMesh->cloud);
        mesh = filteredMesh;
    }
    
    /*************/
    unsigned long long SolidifyImpl::getMesh(std::vector<unsigned char>& mesh)
    {
        pcl::PolygonMesh::Ptr newMesh= boost::make_shared<pcl::PolygonMesh> ();
        getMesh(newMesh);
        mesh = _meshSerializer.serialize(newMesh, _timestamp);

        return _timestamp;
    }

    /*************/
    unsigned long long SolidifyImpl::getMesh(pcl::PolygonMesh::Ptr mesh)
    {
        if (_mesh == nullptr)
            return 0;

        lock_guard<mutex> lock(_mutex);

        // We want to use a cubic grid
        PointXYZRGBNormal minPt, maxPt, dim;
        getMinMax3D(*_cloud, minPt, maxPt);
        dim.x = maxPt.x - minPt.x;
        dim.y = maxPt.y - minPt.y;
        dim.z = maxPt.z - minPt.z;
        float maxDist = std::max(dim.x, std::max(dim.y, dim.z));
        _marchingCubes->setGridResolution((int)(_gridMaxResolution * dim.x / maxDist),
                                          (int)(_gridMaxResolution * dim.y / maxDist),
                                          (int)(_gridMaxResolution * dim.z / maxDist));

        _marchingCubes->reconstruct(*_mesh);
        // Marching cubes produces some NaN vertices, we need to get rid of them
        filterNaNFromMesh(_mesh);
        if (_saveMesh)
        {
            string filename = "solidifiedMesh_" + to_string(_timestamp) + ".obj";
            io::saveOBJFile(filename, *_mesh);
            cout << "Solidify::" << __FUNCTION__ << " - Saved mesh named " << filename << endl;
        }

        mesh->cloud = _mesh->cloud;
        mesh->polygons = _mesh->polygons;

        return _timestamp;
    }

    /*************/
    void SolidifyImpl::setGridResolution(int res)
    {
        if (res <= 0)
            return;

        lock_guard<mutex> lock(_mutex);
        //_marchingCubes->setGridResolution(res, res, res);
        _gridMaxResolution = res;
    }
    
    /*************/
    void SolidifyImpl::setInputCloud(vector<char> cloud, bool compressed)
    {
        lock_guard<mutex> lock(_mutex);

        _cloud = _pclSerializer.deserialize(cloud, compressed, _timestamp);

        _searchTree->setInputCloud(_cloud);
        _marchingCubes->setInputCloud(_cloud);
    }

    /*************/
    void SolidifyImpl::setInputCloud(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud)
    {
        lock_guard<mutex> lock(_mutex);

        _cloud = cloud;

        _searchTree->setInputCloud(_cloud);
        _marchingCubes->setInputCloud(_cloud);
    }

    /*************/
    void SolidifyImpl::setSaveMesh(bool active)
    {
        lock_guard<mutex> lock(_mutex);
        _saveMesh = active;
    }

} // end of namespace
