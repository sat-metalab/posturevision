#include "colorize-impl.h"

#include <cassert>

#include <boost/make_shared.hpp>
#include <pcl/console/print.h>
#include <pcl/io/obj_io.h>

using namespace std;
using namespace pcl;

namespace posture
{
/*********/
ColorizeImpl::ColorizeImpl()
{
    console::setVerbosityLevel(console::L_ERROR);
}

/*********/
ColorizeImpl::~ColorizeImpl()
{
}

/*********/
vector<unsigned char> ColorizeImpl::getTexture(unsigned int& width, unsigned int& height)
{
    width = _mergedWidth;
    height = _mergedHeight;
    return _mergedImage;
}

/*********/
unsigned long long ColorizeImpl::getTexturedMesh(std::vector<unsigned char>& mesh)
{
    pcl::TextureMesh::Ptr tempMesh = boost::make_shared<pcl::TextureMesh>();
    getTexturedMesh(tempMesh);

    mesh = _meshSerializer.serialize(tempMesh, _timestamp);
    return _timestamp;
}

/*********/
unsigned long long ColorizeImpl::getTexturedMesh(pcl::TextureMesh::Ptr mesh)
{
    lock_guard<mutex> lock(_mutex);

    if (_mesh == nullptr)
        return 0;

    if (_dims.size() == 0)
        return _timestamp;

    unsigned int nbrCameras = _dims.size();
    unsigned int maxWidth = 0;
    unsigned int maxHeight = 0;
    // Texture coordinates are merged too, so they have to be translated / scaled to fit in [0, 1]
    for (auto& dim : _dims)
    {
        maxWidth = std::max(maxWidth, dim[0]);
        maxHeight = std::max(maxHeight, dim[1]);
    }

    if (_computeTexCoords)
    {
        // Create the cameras from the dims and calibration
        texture_mapping::CameraVector cameras;
        for (unsigned int i; i < _dims.size(); ++i)
        {
            texture_mapping::Camera camera;
            camera.pose = getTransformationForCamera(i);
            camera.width = _dims[i][0];
            camera.height = _dims[i][1];
            camera.focal_length = _calibrationParams[i].rgb_focal;
            cameras.push_back(camera);
        }

        // Create the UV mapping for each camera
        PointCloud<PointNormal> visible_points;
        _textureMesh = boost::make_shared<TextureMesh>();
        _textureMesh->cloud = _mesh->cloud;
        _textureMesh->tex_polygons = _mesh->tex_polygons;
        _textureMapping.setF(0.1);
        _textureMapping.textureMeshwithMultipleCameras(*_textureMesh, cameras);

        // Merge the mappings together
        _mesh = boost::make_shared<TextureMesh>();
        _mesh->cloud = _textureMesh->cloud;
        _mesh->tex_polygons.push_back(_textureMesh->tex_polygons[0]);

        for (unsigned int i = 1; i < _textureMesh->tex_polygons.size(); ++i)
            for (auto& poly : _textureMesh->tex_polygons[i])
                _mesh->tex_polygons[0].push_back(poly);

        _mesh->tex_coordinates.push_back(vector<Eigen::Vector2f, Eigen::aligned_allocator<Eigen::Vector2f>>());
        for (unsigned int i = 0; i < _dims.size(); ++i)
        {
            float scale = 1.f / (float)nbrCameras * (float)maxWidth / (float)_dims[i][0];
            float shift = 1.f / (float)nbrCameras * (float)i;

            for (auto& coord : _textureMesh->tex_coordinates[i])
            {
                Eigen::Vector2f newCoord;
                newCoord[0] = coord[0] * scale + shift;
                newCoord[1] = coord[1];
                _mesh->tex_coordinates[0].push_back(newCoord);
            }
        }

        // We set the UV for unsorted faces to (0, 0)
        for (auto& coord : _textureMesh->tex_coordinates[_textureMesh->tex_coordinates.size() - 1])
            _mesh->tex_coordinates[0].push_back(Eigen::Vector2f(0.f, 0.f));
    }

    // And we merge all input textures into a single one
    // (we still only support input RGB images)
    unsigned int pixelSize = 3;
    unsigned int mergedSize = maxWidth * maxHeight * pixelSize * nbrCameras;
    if (_mergedImage.size() != mergedSize)
        _mergedImage.resize(mergedSize, 0);

    _mergedWidth = maxWidth * nbrCameras;
    _mergedHeight = maxHeight;

    for (unsigned int i = 0; i < _images.size(); ++i)
    {
        unsigned int shift = maxWidth * i * pixelSize;
        unsigned int step = maxWidth * nbrCameras * pixelSize;
        for (unsigned int y = 0; y < _dims[i][1]; ++y)
            memcpy(_mergedImage.data() + y * step + shift, _images[i].data() + y * _dims[i][0] * pixelSize, _dims[i][0] * pixelSize * sizeof(unsigned char));
    }

    *mesh = *_mesh;
    return _timestamp;
}

/*************/
Eigen::Affine3f ColorizeImpl::getTransformationForCamera(unsigned int index)
{
    auto transformation = _calibrationParams[index].getTransformation();
    Eigen::Affine3f affine(transformation);

    return affine;
}

/*********/
void ColorizeImpl::setComputeTexCoords(bool active)
{
    _computeTexCoords = active;
}

/*********/
void ColorizeImpl::setInput(vector<unsigned char>& mesh, vector<vector<unsigned char>>& images, vector<vector<unsigned int>>& dims)
{
    if (images.size() != dims.size())
        return;
    for (int i = 0; i < dims.size(); ++i)
        if (dims[i].size() != 3)
            return;

    lock_guard<mutex> lock(_mutex);

    TextureMesh::Ptr inputMesh = _meshSerializer.deserializeAsTextured(mesh, _timestamp);
    _mesh = inputMesh;

    _images = images;
    _dims = dims;
}

/*********/
void ColorizeImpl::setInput(TextureMesh::Ptr mesh, std::vector<std::vector<unsigned char>> images, std::vector<std::vector<unsigned int>> dims)
{
    if (images.size() != dims.size())
        return;
    for (int i = 0; i < dims.size(); ++i)
        if (dims[i].size() != 3)
            return;

    lock_guard<mutex> lock(_mutex);

    _mesh = boost::make_shared<TextureMesh>();
    _mesh->cloud = mesh->cloud;
    _mesh->tex_polygons = mesh->tex_polygons;
    _mesh->tex_coordinates = mesh->tex_coordinates;

    _images = images;
    _dims = dims;
}

/*********/
void ColorizeImpl::setInput(PolygonMesh::Ptr mesh, std::vector<std::vector<unsigned char>> images, std::vector<std::vector<unsigned int>> dims)
{
    if (images.size() != dims.size())
        return;
    for (int i = 0; i < dims.size(); ++i)
        if (dims[i].size() != 3)
            return;

    lock_guard<mutex> lock(_mutex);

    _mesh = boost::make_shared<TextureMesh>();
    _mesh->cloud = mesh->cloud;
    _mesh->tex_polygons.push_back(mesh->polygons);

    _images = images;
    _dims = dims;
}

} // end of namespace
