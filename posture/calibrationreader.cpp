#include "calibrationreader.h"

#include <cstring>
#include <fstream>

using namespace std;

namespace posture
{
    /*************/
    CalibrationReader::CalibrationReader(string calibrationPath, string devicesPath)
    {
        loadDevices(devicesPath);
        loadCalibration(calibrationPath);
    }
    
    /*************/
    CalibrationReader::~CalibrationReader()
    {
        xmlCleanupParser();
    }
    
    /*************/
    CalibrationParams CalibrationReader::getCalibrationParams(const int index)
    {
        if (index >= _calibrationParams.size())
            return CalibrationParams();
        return _calibrationParams[index];
    }

    /*************/
    unsigned int CalibrationReader::getSourceNumber()
    {
        return _calibrationParams.size();
    }
    
    /*************/
    void CalibrationReader::readCalibrationAttributes(xmlNode *rootNode, int& calibrationIndex)
    {
        CalibrationParams calibration;
    
        for (xmlNodePtr currentNode = rootNode; currentNode; currentNode = currentNode->next)
        {
            if (currentNode->type == XML_ELEMENT_NODE)
            {
                unsigned int attributeId = 0;
                ClippingShape shape;
                
                for(xmlAttrPtr attribute = currentNode->properties; attribute != NULL; attribute = attribute->next, ++attributeId)
                {
                    if (xmlStrcmp(currentNode->name, (xmlChar*)XML_CAMERA_TAG) == 0)
                    {
                        if (strcmp((const char*)attribute->name, "focal") == 0)
                            calibration.rgb_focal = atof((char*)attribute->children->content);
                        if (strcmp((const char*)attribute->name, "rgbd_distance") == 0)
                            calibration.rgbd_distance = atof((char*)attribute->children->content);
                        if (strcmp((const char*)attribute->name, "scale") == 0)
                            calibration.scale = atof((char*)attribute->children->content);
                    }
                    if (xmlStrcmp(currentNode->name, (xmlChar*)XML_TRANSLATION_TAG) == 0)
                    {
                        if (strcmp((const char*)attribute->name, "x") == 0)
                            calibration.offset_x = atof((char*)attribute->children->content);
                        if (strcmp((const char*)attribute->name, "y") == 0)
                            calibration.offset_y = atof((char*)attribute->children->content);
                        if (strcmp((const char*)attribute->name, "z") == 0)
                            calibration.offset_z = atof((char*)attribute->children->content);
                    }
                    else if (xmlStrcmp(currentNode->name, (xmlChar*)XML_ROTATION_TAG) == 0)
                    {
                        if (strcmp((const char*)attribute->name, "x") == 0)
                            calibration.rot_x = atof((char*)attribute->children->content);
                        if (strcmp((const char*)attribute->name, "y") == 0)
                            calibration.rot_y = atof((char*)attribute->children->content);
                        if (strcmp((const char*)attribute->name, "z") == 0)
                            calibration.rot_z = atof((char*)attribute->children->content);
                    }
                    else if (xmlStrcmp(currentNode->name, (xmlChar*)XML_CLIPPING_TAG) == 0)
                    {
                        if (strcmp((const char*)attribute->name, "x-min") == 0)
                                calibration.min_clip_x = atof((char*)attribute->children->content);
                        if (strcmp((const char*)attribute->name, "x-max") == 0)
                                calibration.max_clip_x = atof((char*)attribute->children->content);
                        if (strcmp((const char*)attribute->name, "y-min") == 0)
                                calibration.min_clip_y = atof((char*)attribute->children->content);
                        if (strcmp((const char*)attribute->name, "y-max") == 0)
                                calibration.max_clip_y = atof((char*)attribute->children->content);
                        if (strcmp((const char*)attribute->name, "z-min") == 0)
                                calibration.min_clip_z = atof((char*)attribute->children->content);
                        if (strcmp((const char*)attribute->name, "z-max") == 0)
                                calibration.max_clip_z = atof((char*)attribute->children->content);
                    }
                    else if (xmlStrcmp(currentNode->name, (xmlChar*)XML_SHAPE_TAG) == 0)
                    {
                        if (strcmp((const char*)attribute->name, "type") == 0)
                            shape.name = std::string((const char*)attribute->children->content);
                        else if (strcmp((const char*)attribute->name, "include") == 0)
                        {
                            if (xmlStrcmp(attribute->children->content, (xmlChar*)"true") == 0)
                                shape.include = true;
                            else
                                shape.include = false;
                        }
                        else
                            shape.params.push_back(atof((char*)attribute->children->content));
                    }
                }
                
                if (xmlStrcmp(currentNode->name, (xmlChar*)XML_SHAPE_TAG) == 0)
                {
                    calibration.shapes.push_back(shape);
                }
    
            }
    
            readCalibrationAttributes(currentNode->children, calibrationIndex);
        }
    
        if (rootNode != NULL)
            if (xmlStrcmp(rootNode->parent->name, (xmlChar*)XML_POINTCLOUD_TAG) == 0)
            {
                calibration.isValid = true;
                if (calibrationIndex < _calibrationParams.size())
                    _calibrationParams[calibrationIndex] = calibration;
                else
                    _calibrationParams.push_back(calibration);

                calibrationIndex++;
            }
    }
    
    /*************/
    void CalibrationReader::loadCalibration(string path)
    {
        xmlDoc *doc = NULL;
        xmlNode *rootElement = NULL;

        // Check if the file exists, otherwise we load the default file
        auto filepath = path;
        if (!ifstream(filepath).good())
            filepath = string(DATADIR) + "/posturevision/datas/default.kvc";
    
        /*Get the root element node */
        if ((doc = xmlReadFile(filepath.c_str(), NULL, 0)) != NULL)
        {
            int calibrationIndex = 0;
            rootElement = xmlDocGetRootElement(doc);
            readCalibrationAttributes(rootElement, calibrationIndex);
            xmlFreeDoc(doc);

            _calibrationFilePath = filepath;
            _loadedSuccessfully = true;
        }
        else
        {
            std::cout << "Could not parse file " << filepath.c_str() << std::endl;
            _calibrationParams.clear();
            _loadedSuccessfully = false;
        }
    }

    /*************/
    void CalibrationReader::reload()
    {
        loadCalibration(_calibrationFilePath);
    }
    
    /*************/
    void CalibrationReader::readDeviceAttributes(xmlNode *rootNode)
    {
        Device device;
        bool manufacturer = false, product = false;
    
        for (xmlNodePtr currentNode = rootNode; currentNode; currentNode = currentNode->next)
        {
           if (currentNode->type == XML_ELEMENT_NODE)
           {
              unsigned int attributeId = -1;
              for(xmlAttrPtr attribute = currentNode->properties; attribute != NULL; attribute = attribute->next, ++attributeId)
              {
                  if(xmlStrcmp(currentNode->name, (xmlChar*)XML_MANUFACTURER) == 0)
                  {
                      device.manufacturer = (char*)attribute->children->content;
                      manufacturer = true;
                  }
                  else if(xmlStrcmp(currentNode->name, (xmlChar*)XML_PRODUCT) == 0)
                  {
                      device.product = (char*)attribute->children->content;
                      product = true;
                  }
                  else
                  {
                      product = false;
                      manufacturer = false;
                      device.manufacturer = "";
                      device.product = "";
                  }
    
                  if(manufacturer && product)
                  {
                      _knownDevices.push_back(device);
    
                      product = false;
                      manufacturer = false;
                      device.manufacturer = "";
                      device.product = "";
                  }
              }
           }
           readDeviceAttributes(currentNode->children);
        }
    }
    
    /*************/
    void CalibrationReader::loadDevices(string path)
    {
        xmlDoc *doc = NULL;
        xmlNode *rootElement = NULL;

        // Check if the file exists, otherwise we load the default file
        auto filepath = path;
        if (!ifstream(filepath).good())
            filepath = string(DATADIR) + "/posturevision/datas/devices.xml";
    
        /*Get the root element node */
        if ((doc = xmlReadFile(filepath.c_str(), NULL, 0)) != NULL)
        {
            rootElement = xmlDocGetRootElement(doc);
            readDeviceAttributes(rootElement);
            xmlFreeDoc(doc);
        }
        else
            std::cout << "Could not parse file " << filepath.c_str() << std::endl;
    }

} // end of namespace
