#include "register.h"
#include "register-impl.h"

using namespace std;

namespace posture
{
    /**********/
    Register::Register()
    {
        _impl = make_shared<RegisterImpl>();
    }

    /**********/
    Register::~Register()
    {
    }

    /**********/
    vector<CalibrationParams> Register::getCalibration() const
    {
        if (!_impl)
            return {};

        return _impl->getCalibration();
    }

    /**********/
    void Register::setInputCloud(const unsigned int index, const vector<char>& cloud, bool compressed) const
    {
        if (!_impl)
            return;

        _impl->setInputCloud(index, cloud, compressed);
    }

    /**********/
    void Register::setInputCloud(const unsigned int index, const pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud) const
    {
        if (!_impl)
            return;

        _impl->setInputCloud(index, cloud);
    }

    /*********/
    void Register::setGuessCalibration(const vector<CalibrationParams>& calibration) const
    {
        if (!_impl)
            return;

        _impl->setGuessCalibration(calibration);
    }

} // end of namespace
