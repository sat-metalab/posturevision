#include "solidifyGPU-impl.h"

#include <iostream>
#include <map>
#include <thread>
#include <future>
#include <unistd.h>
#include <assert.h>

#include <boost/make_shared.hpp>
#include <pcl/io/obj_io.h>
#include <pcl/common/distances.h>

#include "meshmerger.h"

using namespace std;
using namespace pcl;

namespace posture
{
    static string SHADER_BILATERAL_FILTER {R"(
        #version 430 core

        #extension GL_NV_gpu_shader5 : enable
        #extension GL_ARB_compute_shader : enable
        #extension GL_ARB_shader_storage_buffer_object : enable

        #define PI 3.14159265359

        layout(local_size_x = 32, local_size_y = 32) in;

        layout(std430, binding = 0) buffer depthMapBuffer
        {
            unsigned short depthMap[];
        };

        uniform vec2 _size;
        uniform int _kernelShift;
        uniform float _sigmaPos;
        uniform float _sigmaValue;

        float gauss1D(float x, float sigma)
        {
            return exp(-pow(x / sigma, 2.0) / 2.0);
        }

        float gauss2D(vec2 pos, vec2 sigma)
        {
            return gauss1D(pos.x, sigma.x) * gauss1D(pos.y, sigma.y);
        }

        void main(void)
        {
            vec2 pixCoords = gl_WorkGroupID.xy * vec2(32.0) + gl_LocalInvocationID.xy;
            if (pixCoords.x > _size.x || pixCoords.y > _size.y)
                return;

            ivec2 pos = ivec2(int(pixCoords.x), int(pixCoords.y));
            int index = pos.x + pos.y * int(_size.x);

            if (depthMap[index] == uint16_t(0))
                return;

            float filtered = 0.0;
            float samples = 0.0;
            for (int y = pos.y - _kernelShift; y <= pos.y + _kernelShift; ++y)
            {
                if (y < 0 || y >= int(_size.y))
                    continue;

                for (int x = pos.x - _kernelShift; x <= pos.x + _kernelShift; ++x)
                {
                    if (x < 0 || x >= int(_size.x))
                        continue;

                    int neighbourIndex = x + y * int(_size.x);
                    if (depthMap[neighbourIndex] == uint16_t(0))
                        continue;

                    float posCoeff = gauss2D(vec2(x - pos.x, y - pos.y), vec2(_sigmaPos));
                    float valueCoeff = gauss1D(abs(float(depthMap[index]) - float(depthMap[neighbourIndex])), _sigmaValue);
                    float coeff = posCoeff * valueCoeff;
                    filtered += depthMap[neighbourIndex] * coeff;
                    samples += coeff;
                }
            }

            depthMap[index] = uint16_t(filtered / samples);
        }
    )"};

    /*************/
    SolidifyGPUImpl::SolidifyGPUImpl()
    {
        _nThreads = getCPUNbr();

        _mesh = boost::make_shared<PolygonMesh>();
        _resolution << _gridMaxResolution, _gridMaxResolution, _gridMaxResolution;
        _grid = boost::make_shared<gpu::TsdfVolume> (_resolution);
        _grid->setTsdfTruncDist(0.1);

        _glEngine = make_shared<GLEngine>();
        _glEngine->setBufferNumber(1);
        _shaderBilateralFilter = make_shared<Shader>(_glEngine, Shader::prgCompute);
        _shaderBilateralFilter->setSource(SHADER_BILATERAL_FILTER, Shader::compute);
        _shaderBilateralFilter->compileProgram();
    }

    /*************/
    SolidifyGPUImpl::~SolidifyGPUImpl()
    {
        _grid->reset(_resolution);
    }


    /*************/
    unsigned long long SolidifyGPUImpl::getMesh(std::vector<unsigned char>& mesh, bool threaded)
    {
        if (!threaded)
        {
            PolygonMesh::Ptr newMesh = boost::make_shared<PolygonMesh> ();
            _timestamp = getMesh(newMesh);

            // TODO: compression parameters maybe ought to be defined in a more obvious place?
            _meshSerializer.setCompressionMethod(CTM_METHOD_MG2, 1, 0.01);
            mesh = _meshSerializer.serialize(newMesh, _timestamp);  
        }
        else
        {
            vector<PolygonMesh::Ptr> submeshList;
            TRACE(auto timenow = chrono::high_resolution_clock::now();
                  int time_ms = chrono::duration_cast<chrono::milliseconds>(timenow.time_since_epoch()).count();)
            
            _timestamp = getMesh(submeshList);

            TRACE(timenow = chrono::high_resolution_clock::now();
                  int time_ms2 = chrono::duration_cast<chrono::milliseconds>(timenow.time_since_epoch()).count();)
            TRACE(cout << time_ms2-time_ms << "ms for Solidify: getMeshConcurrent"  << endl << endl;)

            // compression parameters maybe ought to be defined in a more obvious place?
            _meshSerializer.setCompressionMethod(CTM_METHOD_MG2, 1, 0.01);
            mesh = _meshSerializer.serialize(submeshList, _timestamp);  

            TRACE(timenow = chrono::high_resolution_clock::now();
                  int time_ms3 = chrono::duration_cast<chrono::milliseconds>(timenow.time_since_epoch()).count();)
            TRACE(cout << time_ms3-time_ms2 << "ms for Solidify: serialization"  << endl;)
        }

        return _timestamp;
    }

    /*************/
    unsigned long long SolidifyGPUImpl::getMesh(pcl::PolygonMesh::Ptr& mesh)
    {
         // execute the Marching Cubes algorithm
        gpu::DeviceArray<PointXYZ> triangles = runMarchingCubes();

        // get output from GPU, weld redundant vertices and make a mesh
        deviceArrayToPolygonMesh(triangles, mesh);

        // clip the shape and remove NaNs
        _filter.applyShapeClipping(mesh, _calibrationParams[0]);
    }

    /*************/
    unsigned long long SolidifyGPUImpl::getMesh(vector<PolygonMesh::Ptr>& submeshList)
    {
        TRACE(auto timenow = chrono::high_resolution_clock::now();
              int time_ms = chrono::duration_cast<chrono::milliseconds>(timenow.time_since_epoch()).count();)

        // execute the Marching Cubes algorithm
        gpu::DeviceArray<PointXYZ> triangles = runMarchingCubes();

        TRACE(timenow = chrono::high_resolution_clock::now();
              int time_ms2 = chrono::duration_cast<chrono::milliseconds>(timenow.time_since_epoch()).count();)
        TRACE(cout << time_ms2-time_ms << "ms for Solidify: Marching Cubes"  << endl;)


        // split output from GPU into n pieces, weld redundant vertices & make n meshes
        submeshList = deviceArrayToPolygonMesh(triangles, _nThreads);

        TRACE(timenow = chrono::high_resolution_clock::now();
              int time_ms3 = chrono::duration_cast<chrono::milliseconds>(timenow.time_since_epoch()).count();)
        TRACE(cout << time_ms3-time_ms2 << "ms for Solidify: welding and polymesh creation" << endl;)

        // clip the shape and remove NaNs from each submesh  (this could also be parallelized)
        for (auto& m: submeshList)
            _filter.applyShapeClipping(m, _calibrationParams[0]);

        TRACE(timenow = chrono::high_resolution_clock::now();
              int time_ms4 = chrono::duration_cast<chrono::milliseconds>(timenow.time_since_epoch()).count();)
        TRACE(cout << time_ms4-time_ms3 << "ms for Solidify: clipping"  << endl;)

    }

    /*************/
    gpu::DeviceArray<PointXYZ> SolidifyGPUImpl::runMarchingCubes()
    {
        if (_mesh == nullptr)
            return 0;

        unique_lock<mutex> lock(_mutex);

        // Update the parameters
        if (_updatedGridParameters)
        {
            _gridMaxSize = max(_gridMaxSizeX, max(_gridMaxSizeY, _gridMaxSizeZ));
            _gridMaxResolution = (int) (_gridMaxSize / _scaledResolution);
            _resolution << (int)(_gridMaxSizeX / _scaledResolution), (int)(_gridMaxSizeY / _scaledResolution), (int)(_gridMaxSizeZ / _scaledResolution);
            _size << _gridMaxSizeX, _gridMaxSizeY, _gridMaxSizeZ;

            _grid->setResolution(_resolution);
            _grid->setSize(_size);
            _grid->setTsdfTruncDist(5 * _gridMaxSize/_gridMaxResolution);

            _updatedGridParameters = false;
        }

        // this is what takes up a lot of memory
        _triangleBuffer.create(3 * 3 * _gridMaxResolution * _gridMaxResolution * _gridMaxResolution);

        _grid->reset(_resolution);
        
        // fill up the cube with constructed TSDF data
        unique_lock<mutex> lockCalibration(_calibrationMutex);
        for (int i = 0; i < _depthMaps.size(); i++)
        {
            auto& calibrationParams = _calibrationParams[i];
            auto trans = _calibrationParams[i].getTranslation();
            auto rot = _calibrationParams[i].getRotation();

            if (_depthMaps[i].size() != 0)
            {
                // Filter this depth map
                _glEngine->setBuffer(_depthMaps[i], 0, 1);
                _shaderBilateralFilter->setUniform("_size", {(float)_widths[i], (float)_heights[i]});
                _shaderBilateralFilter->setUniform("_kernelShift", {(int)_kernelSize / 2});
                _shaderBilateralFilter->setUniform("_sigmaPos", {(float)_sigmaPos});
                _shaderBilateralFilter->setUniform("_sigmaValue", {(float)_sigmaValue});
                _shaderBilateralFilter->setComputeSize(_widths[i] / 32 + 1, _heights[i] / 32 + 1);
                _glEngine->setShader(_shaderBilateralFilter);
                _glEngine->run();
                _depthMaps[i] = _glEngine->getFeedbackBuffer<unsigned short>(0);
                
                // Upload this depth map and integrate its data to the TSDF
                _depthMapsGPU[i].upload(_depthMaps[i].data(), _widths[i] * sizeof(unsigned short), _heights[i], _widths[i]);
                gpu::integrateTsdfVolume(_depthMapsGPU[i], calibrationParams.rgb_focal, _widths[i] / 2, _heights[i] / 2, _size, _resolution, rot, trans, i, _grid);
            }
        }

        // fill holes
        _volumetricDiffusion.run(*_grid);

        // now we're finally ready to roll!
        return _marchingCubes.run(*_grid, _resolution, _triangleBuffer);
    }   

    /*************/
    void SolidifyGPUImpl::setGridResolution(float res)
    {
        if (res <= 0)
            return;

        unique_lock<mutex> lock(_mutex);

        _scaledResolution = res;
        _updatedGridParameters = true;
    }

    /*************/
    void SolidifyGPUImpl::setGridSize(float size)
    {
        if (size <= 0)
            return;

        unique_lock<mutex> lock(_mutex);
        _gridMaxSizeX = size;
        _gridMaxSizeY = size;
        _gridMaxSizeZ = size;

        _updatedGridParameters = true;
    }

    /*************/
    void SolidifyGPUImpl::setGridSizeX(float size)
    {
        if (size <= 0)
            return;

        unique_lock<mutex> lock(_mutex);
        _gridMaxSizeX = size;
        _updatedGridParameters = true;
    }

    /*************/
    void SolidifyGPUImpl::setGridSizeY(float size)
    {
        if (size <= 0)
            return;

        unique_lock<mutex> lock(_mutex);
        _gridMaxSizeY = size;
        _updatedGridParameters = true;
    }

    /*************/
    void SolidifyGPUImpl::setGridSizeZ(float size)
    {
        if (size <= 0)
            return;

        unique_lock<mutex> lock(_mutex);
        _gridMaxSizeZ = size;
        _updatedGridParameters = true;
    }

    /*************/
    void SolidifyGPUImpl::setDepthMapNbr (unsigned int sourceNbr)
    {
        _depthMaps.resize(sourceNbr);
        _depthMapsGPU.resize(sourceNbr);
        _widths.resize(sourceNbr);
        _heights.resize(sourceNbr);
    }

    /*************/
    void SolidifyGPUImpl::setHoleFillingIterations(int iter)
    {
        unique_lock<mutex> lock(_mutex);
        _volumetricDiffusion.setNumIterations(iter);
    }

    /*************/
    void SolidifyGPUImpl::setInputDepthMap(int index, const std::vector<unsigned char>& depth, int width, int height)
    {
        unique_lock<mutex> lock(_mutex);
        if (index >= _depthMaps.size())
        {
            cout << index << " is not a correct value for a camera index."<< endl;
            return;
        }

        _depthMaps[index].resize(depth.size() / 2);  // SolidifyGPU depth map is a vector of 2-byte unsigned shorts 
        memcpy(_depthMaps[index].data(), depth.data(), depth.size());
        _widths[index] = width;
        _heights[index] = height;
    }

    /*************/
    void SolidifyGPUImpl::setInputDepthMap(int index, const std::vector<unsigned short>& depth, int width, int height)
    {
        unique_lock<mutex> lock(_mutex);
        if (index >= _depthMaps.size())
        {
            cout << index << " is not a correct value for a camera index."<< endl;
            return;
        }
        
        _depthMaps[index] = depth;
        _widths[index] = width;
        _heights[index] = height;
    }

    /*************/
    void SolidifyGPUImpl::setSaveMesh(bool active)
    {
        unique_lock<mutex> lock(_mutex);
        _saveMesh = active;
    }

    /*************/
    int SolidifyGPUImpl::getCPUNbr()
    {
        // We limit the number of threads to 16, as some archs seem to give absurdly large numbers
        int nprocessors = std::min(std::max(sysconf(_SC_NPROCESSORS_CONF), 1l), 16l);
        return nprocessors;
    }

    /*************/
    void SolidifyGPUImpl::deviceArrayToPolygonMesh (const gpu::DeviceArray<PointXYZ>& array, PolygonMesh::Ptr& mesh)
    {
        // takes the output of Marching Cubes on the GPU, 
        // reduces vertex duplication, and writes output into a polygon mesh

        // the device array is just a list of triangle vertices
        int nbrVertices = array.size();
       
        // get the vertex data from the GPU 
        vector<PointXYZ> vertexList(nbrVertices);
        array.download(vertexList.data());

        // weld redundant vertices and make a polygon mesh 

        // option 1
        // mesh = vertexListToPolygonMesh(vertexList, 0, nbrVertices);

        // option 2
        // vector<PolygonMesh::Ptr> submeshList = deviceArrayToPolygonMesh(array, 1);        
        // mesh = submeshList[0];

        // option 3
        // split into _nThreads tasks
        vector<PolygonMesh::Ptr> submeshList = deviceArrayToPolygonMesh(array, _nThreads);

        // merge the meshes
        
        TRACE(auto timenow = chrono::high_resolution_clock::now();
              int time_ms = chrono::duration_cast<chrono::milliseconds>(timenow.time_since_epoch()).count();)
       
        posture::MeshMerger mm(_nThreads);
        mm.setApplyCalibration(false);
        int i=0;
        for (auto m: submeshList)
            mm.setInputMesh(i++, m);

        mm.getMesh(mesh);
 

        TRACE(timenow = chrono::high_resolution_clock::now();
              int time_ms2 = chrono::duration_cast<chrono::milliseconds>(timenow.time_since_epoch()).count();)
        TRACE(        cout << " Solidify: time elapsed during merger is " << time_ms2-time_ms << endl;)

    }


    /*************/
    vector<PolygonMesh::Ptr> SolidifyGPUImpl::deviceArrayToPolygonMesh (const gpu::DeviceArray<PointXYZ>& array, int nSubmeshes)
    {
        // fill a vector with n Submeshes of a mesh represented in the GPU
        // the device array is just a list of triangle vertices
        // each vertex is listed in the device array each time it occurs
        
        int nbrVertices = array.size();
        int nbrPolys = nbrVertices / 3;
        
        // determine how many polygons to put in each submesh
        int polysPerSubmesh = nbrPolys / nSubmeshes;  // e.g. 10 000 / 3 = 3 333 - first mesh will be slightly larger
        vector<int> subArraySizes(nSubmeshes, polysPerSubmesh);
        subArraySizes[0] = nbrPolys - (nSubmeshes - 1) * polysPerSubmesh; // this should equal polysPerSubmesh + nbrPolys % nSubmeshes 
        assert(subArraySizes[0] == polysPerSubmesh + nbrPolys % nSubmeshes );

        TRACE(cerr << " Array sizes: ";)
        for (auto& e : subArraySizes)
        {
            e = 3 * e;  // number of vertex elements in device array
            TRACE(cerr << e << " ";)
        }
        TRACE(cerr << endl;)

        // get the vertex data from the GPU
        vector<PointXYZ> vertexList(nbrVertices);
        array.download(vertexList.data());
        
        // now split it into subarrays and get the mesh from each
        int positionStart = 0;
        
        std::vector<std::future<PolygonMesh::Ptr> > futureMeshes;
        std::vector<PolygonMesh::Ptr> submeshList;
        
        for (int nMesh = 0, positionStart = 0; nMesh < nSubmeshes; ++nMesh)
        {
            //submeshList.push_back(
            //      vertexListToPolygonMesh (vertexList, positionStart, subArraySizes[nMesh]) );
            futureMeshes.push_back(std::async(std::launch::async,
                    vertexListToPolygonMesh, vertexList, positionStart, subArraySizes[nMesh]) );
            positionStart += subArraySizes[nMesh];
        }
        for (auto& f: futureMeshes)
            submeshList.push_back(f.get());

        return submeshList;
    }
    
    /*************/
    PolygonMesh::Ptr SolidifyGPUImpl::vertexListToPolygonMesh(const vector<PointXYZ>& vertexList, int begin, int nbrVertices)
    {
        // generates a polygon mesh from vertexList[begin:begin+n] (after removing redundancy)
        // (could be rewritten with iterators) 

        // the mesh object we will return:
        PolygonMesh::Ptr mesh = boost::make_shared<pcl::PolygonMesh> (); 

        ///////////////////////////////////////
        // Weld vertices that recur, to reduce redundancy
        // (See http://hamelot.io/programming/welding-triangles/ for a GPU algorithm)

        // we will put vertex positions into the mesh point cloud in a later step
        PointCloud<PointXYZ> compactPointCloud;

        int nbrPolys = nbrVertices / 3;
        mesh->polygons.clear();
        mesh->polygons.resize(nbrPolys);

        vector<int> newIndex;  // table associating old vertex ID to new
        newIndex.reserve(nbrVertices);

        // how far back to search for this vertex (should we parallelize this search?)
        // 511: cut by factor 5; 63: cut by factor 3.3; 15: cut by 2.8.  maximum possible is around 5.8
        // An improved implementation might use a hash table for faster lookups.
        const int maxSearchbackLen = 100;
        TRACE(/*cerr << " vLTPM: searchBack = " << maxSearchbackLen;*/)

        for (int i = begin; i < begin + nbrVertices; ++i)
        {
            PointXYZ vertex = vertexList[i];

            int searchbackLen = (compactPointCloud.size() > maxSearchbackLen ? maxSearchbackLen : compactPointCloud.size() );
            
            int newVertexIndex = find_if(compactPointCloud.end() - searchbackLen,
                                         compactPointCloud.end(),
                                         [&vertex](const PointXYZ& p) -> bool {
                                            return (pcl::squaredEuclideanDistance(vertex,p) < 1e-12);
                                            // alternatives: Manhattan l1 distance?  int(1000*v)=int(1000*p) ?
                                         }) - compactPointCloud.begin();
            
            // If vertex was not found, push it 
            if (newVertexIndex == compactPointCloud.size())
                compactPointCloud.push_back(vertex);

            // either way, record its new index  (TODO: build connectivity here)
            // newIndex.push_back(newVertexIndex);

            int currentPoly = (i - begin) / 3;
            mesh->polygons[currentPoly].vertices.push_back(newVertexIndex);

            /* SEARCHING BACKWARDS VERSION (might be faster for large N, as vertex is often recent)
            int i, pos;
            for (i = 0; i < searchbackLen; ++i) // start from the back
            {
                pos = compactPointCloud.size() - 1 - i; 
                if (pcl::squaredEuclideanDistance(compactPointCloud[pos],vertex)< 1e-12)
                    break;
            }
             
            if (i == searchbackLen)
            {
                pos = compactPointCloud.size();
                compactPointCloud.push_back(vertex);               
            }

            newIndex.push_back(pos);
            */
        }

        TRACE(/*cerr << " // initial array size:" << nbrVertices << " // after duplicate removal: " << compactPointCloud.size() << " // ratio: " << float(nbrVertices)/compactPointCloud.size() << endl;*/) // ratio ideally approaches 6x smaller

        // write the compacted point cloud 
        toPCLPointCloud2 (compactPointCloud, mesh->cloud);

        // now build the connectivity
        /* already done!
        for (int i = 0; i < nbrPolys; ++i)
        {
            mesh->polygons[i].vertices.push_back(newIndex[3 * i]);
            mesh->polygons[i].vertices.push_back(newIndex[3 * i + 1]);
            mesh->polygons[i].vertices.push_back(newIndex[3 * i + 2]);
        }
        */

        return mesh;
    } // deviceArrayToPolygonMesh()

} // end of namespace
