#include "meshmerger-impl.h"

#include <iostream>
#include <pcl/console/print.h>

using namespace std;
using namespace pcl;
using namespace Eigen;

namespace posture
{
/*************/
MeshMergerImpl::MeshMergerImpl(const unsigned int meshNbr) : _nbrSources(meshNbr)
{
    console::setVerbosityLevel(console::L_ERROR);
    _meshes.resize(_nbrSources);
    _rawMeshes.resize(_nbrSources);
}

/*************/
MeshMergerImpl::~MeshMergerImpl()
{
    lock_guard<mutex> lock(_mutex);
}

/*************/
void MeshMergerImpl::setInputMesh(const unsigned int index, const vector<unsigned char>& mesh)
{
    std::lock_guard<std::mutex> lock(_inputMutex);
    if (index < _nbrSources && _rawMeshes.size() > index)
        _rawMeshes[index] = mesh;
}

/*************/
void MeshMergerImpl::setInputMesh(const unsigned int index, const pcl::PolygonMesh::Ptr mesh)
{
    // creates a TextureMesh with dummy textures from the PolygonMesh, and stores it
    std::lock_guard<std::mutex> lock(_mutex);
    pcl::TextureMesh::Ptr tempMesh = boost::make_shared<TextureMesh>();
    tempMesh->cloud = mesh->cloud;
    tempMesh->tex_polygons.push_back(mesh->polygons);

    tempMesh->tex_coordinates.resize(1);
    for (auto& poly : tempMesh->tex_polygons[0])
    {
        for (auto& v : poly.vertices)
        {
            tempMesh->tex_coordinates[0].emplace_back(Eigen::Vector2f(0.0f, 0.0f));
        }
    }

    if (index < _nbrSources && _meshes.size() > index)
    {
        _meshes[index] = tempMesh;
        _rawMeshes[index].clear();
    }
}

/*************/
void MeshMergerImpl::setInputMesh(const unsigned int index, const pcl::TextureMesh::Ptr mesh)
{
    std::lock_guard<std::mutex> lock(_mutex);
    if (index < _nbrSources && _meshes.size() > index)
        _meshes[index] = mesh;
}

/*************/
void MeshMergerImpl::setInputMesh(const unsigned int index, vector<unsigned char>&& mesh)
{
    std::lock_guard<std::mutex> lock(_inputMutex);

    if (index < _nbrSources && _rawMeshes.size() > index)
        _rawMeshes[index] = std::move(mesh);
}

/*************/
unsigned long long MeshMergerImpl::getMesh(std::vector<unsigned char>& mesh)
{
    auto newMesh = boost::make_shared<pcl::TextureMesh>();
    getMesh(newMesh);
    mesh = _serializer.serialize(newMesh, _mergedTimestamp);
    return _mergedTimestamp;
}

/*************/
unsigned long long MeshMergerImpl::getMesh(pcl::TextureMesh::Ptr outMesh)
{
    std::lock_guard<std::mutex> lock(_mutex);

    if (_nbrSources == 0)
        return 0;
    else
    {
        _mesh = boost::make_shared<TextureMesh>();
        _mesh->tex_polygons.resize(1);
        _mesh->tex_coordinates.resize(1);

        PointCloud<PointNormal>::Ptr mergedCloud = boost::make_shared<PointCloud<PointNormal>>();

        unique_lock<mutex> lock(_inputMutex);

        // deserialize meshes if they were given as serializations
        for (unsigned int i = 0; i < _nbrSources; ++i)
            if (_rawMeshes[i].size() > 0)
                _meshes[i] = _serializer.deserializeAsTextured(_rawMeshes[i], _timestamps[i]);

        unsigned int index = 0;
        for (auto& mesh : _meshes)
        {
            if (!mesh)
                continue;

            unsigned int verticesNbr = mergedCloud->size();

            PointCloud<PointNormal>::Ptr pointCloud = boost::make_shared<PointCloud<PointNormal>>();
            fromPCLPointCloud2(mesh->cloud, *pointCloud);
            if (_doApplyCalibration)
                transformPointCloudWithNormals(*pointCloud, *pointCloud, _matTransforms[index]);
            *mergedCloud += *pointCloud;

            if (mesh->tex_polygons.size() != 0)
                for (auto& poly : mesh->tex_polygons[0])
                {
                    Vertices tPoly;
                    for (auto& vertex : poly.vertices)
                        tPoly.vertices.push_back(vertex + verticesNbr);
                    _mesh->tex_polygons[0].push_back(tPoly);
                }

            if (mesh->tex_coordinates.size() != 0)
            {
                // Texture coordinates are scaled, textures are considered to have the same dimensions
                float scale = 1.f / (float)_nbrSources;
                float shift = scale * index;

                for (auto& tex : mesh->tex_coordinates[0])
                {
                    Eigen::Vector2f coords;
                    coords[0] = tex[0] * scale + shift;
                    coords[1] = tex[1];
                    _mesh->tex_coordinates[0].push_back(coords);
                }
            }
            index++;
        }

        toPCLPointCloud2(*mergedCloud, _mesh->cloud);

        // clip and remove NaNs
        // if (_doApplyCalibration)
        //    _filter.applyShapeClipping(_mesh, _calibrationParams[0]);

        *outMesh = *_mesh;

        return _mergedTimestamp;
    }
}

/*************/
unsigned long long MeshMergerImpl::getMesh(pcl::PolygonMesh::Ptr mesh)
{
    pcl::TextureMesh::Ptr tempMesh = boost::make_shared<pcl::TextureMesh>();
    tempMesh->tex_polygons.resize(1);
    tempMesh->tex_coordinates.resize(1);
    _mergedTimestamp = getMesh(tempMesh);

    if (tempMesh->tex_polygons.size() != 0)
    {
        mesh->polygons.resize(1);
        mesh->cloud = tempMesh->cloud;
        mesh->polygons = tempMesh->tex_polygons[0];
    }

    return _mergedTimestamp;
}

/*************/
void MeshMergerImpl::start()
{
    std::lock_guard<std::mutex> lock(_mutex);

    // If _nbrSources == 0, we output noise
    if (_nbrSources == 0)
        return;

    if (_meshes.size() != _nbrSources)
    {
        _meshes.resize(_nbrSources);
        _rawMeshes.resize(_nbrSources);
    }

    for (unsigned int i = 0; i < _nbrSources; ++i)
    {
        _inputMutex.lock();
        _meshes[i] = boost::make_shared<TextureMesh>();
        _rawMeshes[i] = vector<unsigned char>();
        _inputMutex.unlock();
        _timestamps.push_back(0);
    }

    initializeTransformation();
}

/*************/
void MeshMergerImpl::stop()
{
}

/*************/
void MeshMergerImpl::initializeTransformation()
{
    for (unsigned int i = 0; i < _nbrSources; ++i)
    {
        if (i < _matTransforms.size())
            _matTransforms[i] = _calibrationParams[i].getTransformation();
        else
            _matTransforms.push_back(_calibrationParams[i].getTransformation());
    }
}

} // end of namespace
