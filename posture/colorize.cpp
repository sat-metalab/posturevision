#include "colorize.h"
#include "colorize-impl.h"

using namespace std;

namespace posture
{
    /*********/
    Colorize::Colorize()
    {
        _impl = make_shared<ColorizeImpl>();
    }

    /*********/
    Colorize::~Colorize()
    {
    }

    /*********/
    vector<unsigned char> Colorize::getTexture(unsigned int& width, unsigned int& height) const
    {
        if (!_impl)
            return vector<unsigned char>();

        return _impl->getTexture(width, height);
    }

    /*********/
    unsigned long long Colorize::getTexturedMesh(std::vector<unsigned char>& mesh) const
    {
        if (!_impl)
            return 0;

        return _impl->getTexturedMesh(mesh);
    }

    /*********/
    unsigned long long Colorize::getTexturedMesh(pcl::TextureMesh::Ptr mesh) const
    {
        if (!_impl)
            return 0;

        return _impl->getTexturedMesh(mesh);
    }

    /*********/
    void Colorize::setCalibration(const vector<CalibrationParams>& calibrations) const
    {
        if (!_impl)
            return;
        
        return _impl->setCalibration(calibrations);
    }

    /*********/
    void Colorize::setCompressMesh(bool active) const
    {
        if (!_impl)
            return;

        _impl->setCompressMesh(active);
    }

    /*********/
    void Colorize::setComputeTexCoords(bool active) const
    {
        if (!_impl)
            return;

        _impl->setComputeTexCoords(active);
    }

    /*********/
    void Colorize::setInput(vector<unsigned char> mesh, vector<vector<unsigned char>> images, vector<vector<unsigned int>> dims) const
    {
        if (!_impl)
            return;

        _impl->setInput(mesh, images, dims);
    }

    /*************/
    void Colorize::setInput(pcl::TextureMesh::Ptr mesh, std::vector<std::vector<unsigned char>> images, std::vector<std::vector<unsigned int>> dims) const
    {
        if (!_impl)
            return;

        _impl->setInput(mesh, images, dims);
    }

    /*************/
    void Colorize::setInput(pcl::PolygonMesh::Ptr mesh, std::vector<std::vector<unsigned char>> images, std::vector<std::vector<unsigned int>> dims) const
    {
        if (!_impl)
            return;

        _impl->setInput(mesh, images, dims);
    }

} // end of namespace
