#include "pointcloudmerger.h"
#include "pointcloudmerger-impl.h"

using namespace std;

namespace posture
{
    /*************/
    PointCloudMerger::PointCloudMerger()
    {
        _impl = make_shared<PointCloudMergerImpl>();
    }
    
    /*************/
    PointCloudMerger::~PointCloudMerger()
    {
    }
    
    /*************/
    void PointCloudMerger::setCalibration(const vector<CalibrationParams>& calibrations) const
    {
        if (!_impl)
            return;
        
        return _impl->setCalibration(calibrations);
    }

    /*************/
    void PointCloudMerger::setCloudNbr (unsigned int cloudNbr) const
    {
        if (!_impl)
            return;

        _impl->setCloudNbr (cloudNbr);
    }
    
    /*************/
    void PointCloudMerger::setCompression(bool compress) const
    {
        if (!_impl)
            return;
        
        _impl->setCompression(compress);
    }

    /*************/
    void PointCloudMerger::setDownsampling(bool active, float resolution) const
    {
        if (!_impl)
            return;

        _impl->setDownsampling(active, resolution);
    }

    /*************/
    void PointCloudMerger::setRandomNoiseSize(const unsigned int size) const
    {
        if (!_impl)
            return;

        _impl->setRandomNoiseSize(size);
    }

    /*************/
    void PointCloudMerger::setInputCloud(const unsigned int index, const vector<char>& cloud, bool compressed) const
    {
        if (!_impl)
            return;

        _impl->setInputCloud(index, cloud, compressed);
    }

    /*************/
    void PointCloudMerger::setInputCloud(const unsigned int index, const pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud) const
    {
        if (!_impl)
            return;

        _impl->setInputCloud(index, cloud);
    }
    
    /*************/
    void PointCloudMerger::setLoadedPCD(std::string pcdPath) const
    {
        if (!_impl)
            return;

        _impl->setLoadedPCD(pcdPath);
    }
    
    /*************/
    void PointCloudMerger::setSaveCloud(bool active) const
    {
        if (!_impl)
            return;

        _impl->setSaveCloud(active);
    }
    
    /*************/
    unsigned long long PointCloudMerger::getCloud(std::vector<char>& cloud) const
    {
        if (!_impl)
            return 0;

        return _impl->getCloud(cloud);
    }

    /*************/
    unsigned long long PointCloudMerger::getCloud(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud) const
    {
        if (!_impl)
            return 0;

        return _impl->getCloud(cloud);
    }

    /*************/
    void PointCloudMerger::setSaveSeparately (const bool saveSeparately) const
    {
        _impl->setSaveSeparately(saveSeparately);
    }
    
    /*************/
    void PointCloudMerger::start() const
    {
        if (!_impl)
            return;

        return _impl->start();
    }
    
    /*************/
    void PointCloudMerger::stop() const
    {
        if (!_impl)
            return;

        return _impl->stop();
    }

} // end of namespace
